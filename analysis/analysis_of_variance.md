# 定義

## 分散分析（analysis of variance）とは
多次元の[標本データ](../base/sample.md)について、その成分間の関係性（独立性・従属性）について調査を行う方法の一つ。ある成分（目的変数）が他の成分からなる式（説明変数）の結合で表せると仮定した上で、その当てはまりの良さを定量的に分析することを分散分析と呼ぶ。結果は、分散分析表と呼ばれる表で表すことが多い。分散分析という名称は、目的変数を、説明変数によって説明される部分と残差によって説明される部分に分けた時に、それぞれの部分の分散をもって、どちらの部分の影響が大きいかを分析評価することから呼ばれる。

- 一般的には、分散分析は、モデルの係数ベクトルを求めた（回帰分析）後に行われ、下記のn元配置分散分析と区別して回帰分散分析と呼ぶ場合がある。
- 特に、「線形結合で表せる（線形従属性がある）」かつ「残差が多変量正規分布に従う」と仮定した場合を、[一般線形モデル](../base/general_linear_model.md)と呼び、その仮定で回帰分析を行った結果について分散分析を行うことを、線形回帰分散分析と呼ぶ。

## n元配置分散分析
説明変数が質的変数のみである場合の分散分析を、その説明変数の数nを用いてn元配置分散分析と呼ぶ。単に分散分析という場合、このn元配置分散分析を指す場合もある。説明変数が質的変数のみである場合は、回帰分析を経ることなくモデルの係数ベクトルが定まる為、回帰分散分析とは呼ばず、n元配置分散分析と呼ぶ。

---
# 線形回帰分散分析
[一般線形モデル](../base/general_linear_model.md)と[回帰分析](regression_analysis.md)定義の記法を用いる。線形回帰の当てはまりの良さを評価する為に、$`y`$をサイズnの標本としてみたときの分散$`V_{y}`$（注：確率変数と見た時の分散（分散共分散行列）とは異なることに注意）について、分解を行う。

## 分散の分解

- 下記の通り$`y,\check{y},\check{e}`$をサイズnのデータとしてみた時の変動を定義する。
    - $`S_{y} \equiv \sum_k(Y_k-\frac{1}{n}\sum_kY_k)^2 = |y-\frac{1}{n}Uy|^2`$
    - $`S_{\check{y}} \equiv \sum_k(\check{Y}_k-\frac{1}{n}\sum_k \check{Y}_k)^2 =|\check{y}-\frac{1}{n}U\check{y}|^2=|\check{y}-\frac{1}{n}UZy|^2 = |\check{y}-\frac{1}{n}Uy|^2`$
    - $`S_{\check{e}} \equiv \sum_k((Y_k-\check{Y}_k)-\frac{1}{n}\sum_k (Y_k-\check{Y}_k))^2 =|y-\check{y}-\frac{1}{n}U(y-\check{y})|^2 = |y-\check{y}|^2`$
- yの変動は下記の通り分解できる。
    - $`S_{y}=S_{\check{e}}+S_{\check{y}}`$
    - 証明
        - $`|y-\frac{1}{n}Uy|^2=|y-\check{y}+\check{y}-\frac{1}{n}Uy|^2=|y-\check{y}|^2+|\check{y}-\frac{1}{n}Uy|^2-2(y-\check{y})^{\tau}(\check{y}-\frac{1}{n}Uy)`$
        - 上式の第３項は、$`\check{y}=U_{x}\check{a}=Zy`$を用いると、ゼロ行列であることがわかる。
            - $`(y-\check{y})^{\tau}(\check{y}-\frac{1}{n}Uy)=(y^{\tau}-y^{\tau}Z)(Zy-\frac{1}{n}Uy)=y^{\tau}(I-Z)(Z-\frac{1}{n}U)y=y^{\tau}Oy=O`$
- $`S_{y}`$の自由度はn-1、$`S_{\check{y}}`$の自由度はp、$`S_{e}`$の自由度はn-1-pであるから、（不偏）分散はそれぞれ、
    - $`V_{y}=\frac{S_{y}}{n-1}`$
    - $`V_{\check{y}}=\frac{S_{\check{y}}}{p}`$
    - $`V_{\check{e}}=\frac{S_{\check{e}}}{n-1-p}`$
- 従って、（不偏）分散は以下のように分解できる。
    - $`V_{y}=\frac{p}{n-1}V_{\check{y}}+\frac{n-1-p}{n-1}V_{\check{e}}`$

## 決定係数の定義

- 回帰の当てはまりの良さを評価する指標として決定係数$`R^2`$を以下の式で定義する。
     - $`R^2 \equiv \frac{S_{\check{y}}}{S_y}=1-\frac{S_{\check{e}}}{S_y}`$
     - これは[重相関係数](regression_analysis.md)Rの自乗に等しい。
- また自由度調整済み決定係数$`R_f^2`$を以下の式で定義する。
     - $`R_f^2 \equiv 1-\frac{V_{\check{e}}}{V_y}`$

## 回帰のF検定
どの説明変数も目的変数に影響しないという仮説、即ち$`a=\left(\begin{array}{c} a_0 \\ 0 \\ \vdots \\ 0 \end{array}\right)`$と仮定する（帰無仮説）。

帰無仮説の下では、モデル式は$`y = a_0u+\varepsilon`$となる。この仮定のもとで、偏差ベクトル及び残差ベクトルの期待値と分散を計算すると下記になる。

|確率変数|期待値:$`E[\cdot]`$|分散共分散行列:$`V[\cdot]`$|
|-|-|-|
|$`y`$|$`a_0u`$|$`\sigma^2I`$|
|$`\check{y}=Zy`$|$`a_0u`$|$`\sigma^2Z`$|
|$`y_d=(I-\frac{1}{n}U)Y`$|$`0`$|$`\sigma^2(I-\frac{1}{n}U)`$|
|$`\check{y_d}=(Z-\frac{1}{n}U)Y`$|$`0`$|$`\sigma^2(Z-\frac{1}{n}U)`$|
|$`y-\check{y}=(I-Z)y`$|$`0`$|$`\sigma^2(I-Z)`$|

- 従って$`y_d,\check{y_d},y-\check{y}`$は、下記の補題からわかる通りカイ2乗分布に従う。
- なお、自由度の計算に当たっては、ベキ等行列のランクとトレースは等しいことを用いる
    - $`rank(I-\frac{1}{n}U)=tr(I-\frac{1}{n}U)=tr(I)-tr(\frac{1}{n}U)=n-rank(\frac{1}{n}U)=n-1`$
    - $`rank(Z-\frac{1}{n}U)=tr(Z-\frac{1}{n}U)=tr(Z)-tr(\frac{1}{n}U)=P+1-rank(\frac{1}{n}U)=p`$
    - $`rank(I-Z)=tr(I-Z)=tr(I)-tr(Z)=n-(p+1)`$

|カイ2乗分布に従う確率変数|自由度|
|-|-|
|$`\frac{1}{\sigma^2}{\|y_d\|}^2`$|$`rank(I-\frac{1}{n}U)=n-1`$|
|$`\frac{1}{\sigma^2}\|\check{y_d}\|^2`$|$`rank(Z-\frac{1}{n}U)=p`$|
|$`\frac{1}{\sigma^2}\|y-\check{y}\|^2`$|$`rank(I-Z)=n-p-1`$


従って、帰無仮説の下では統計量$`\frac{|\check{y_d}|^2}{|y-\check{y}|^2}=\frac{y^{\tau}(Z-\frac{1}{n}U)y}{y^{\tau}(I-Z)y}`$は自由度(p,n-p-1)のF分布に従う。統計量がF分布の上側（有意水準[%]）点より大きい場合、帰無仮説は棄却され、回帰に意味があると言える。

### 補題

- 確率変数ベクトル$`v\sim N(0,\sigma^2I)`$、非確率変数であるベキ等行列Cに対して、$`\frac{1}{\sigma^2}\|Cv\|^2`$は自由度$`rank(C)`$のカイ2乗分布に従う。
    - 証明
        - $`\frac{1}{\sigma}Cv\sim N(0,C)`$であるから、[正規分布](../distribution/normal.md)に記載の関係を利用すると、下記が成り立つ。
            - $`(\frac{1}{\sigma}Cv)^{\tau}C^+(\frac{1}{\sigma}Cv) \sim \chi^2(rank(\sigma^2C))`$
        - ベキ等行列の疑似逆行列はベキ等行列自身であることを用いて計算すると、
            - $`(\frac{1}{\sigma}Cv)^{\tau}C^+(\frac{1}{\sigma}Cv)=\frac{1}{\sigma^2}(Cv)^{\tau}CCv=\frac{1}{\sigma^2}(Cv)^{\tau}Cv=\frac{1}{\sigma^2}\|Cv\|^2`$

---
# n元配置分散分析
## 1元配置分散分析

- 質的変数$`x`$１つを説明変数、yを目的変数とする。xは、水準数が$`h`$であり、水準$`H_1,\cdots,H_h`$のいずれかろ等しいものとする。n回の測定を行い、n個のデータ$`(x_1,y_1),\cdots,(x_n,y_n)`$を得たとする。これを各水準毎に分類した個数を$`N_1,\cdots,N_h`$（$`\sum N_k = n`$）とする。
    - 全体でのyの平均値を$`\bar{y}`$と表記する。
    - 各水準ごとにyの平均値をとった結果を$`\mu_1,\cdots,\mu_{h}`$と表記する
    - 標本の中で水準kのデータの割合$`p_k=\frac{N_k}{n}`$と表記する。
    - 下記が成り立つ
        - $`\bar{y} = \sum \mu_k p_k`$
- 上記の定義のもとで、xの値からyの最尤推定値$`\hat{y}`$を求める。誤差$`\epsilon=y-\hat{y}`$が正規分布$`N(0,\sigma^2)`$に従うとの仮定のもとで、下記となる
    - $`\hat{y} = \mu(x)`$
        - $`\mu(x)=\mu_1\ (if\ x=H_1),\cdots,\mu(x)=\mu_h\ (if\ x=H_h)`$
    - ダミー変数$`s_1(x),\cdots,s_h(x)`$（$`s_k(x) = 1\ (if\ x=H_k), s_k(x)=0\ (if\ x \neq H_k)`$）を用いれば、下記になる。
        - $`\hat{y} = \mu_1s_1(x)+\cdots+\mu_{h}s_h(x)`$
    - 証明は後述の補題を参照。
- ここで帰無仮説として「xはyに影響しない」という命題を検定してみる。帰無仮説のもとでの最尤推定値は$`\check{y}=\bar{y}`$となる。 
    - 帰無仮説のもとで、下記が成り立つ。
        - $`y_1,\cdots,y_n \sim N(\bar{y},\sigma^2)`$
        - $`\mu_1 \sim N(\bar{y},\frac{\sigma^2}{N_1}),\cdots,\mu_h \sim N(\bar{y},\frac{\sigma^2}{N_h})`$
        - 従って
            - $`\displaystyle \frac{\sum_{k=1}^n(y_k-\bar{y})^2}{\sigma^2} \sim \chi^2(n-1)`$
            - $`\displaystyle \frac{\sum_{j=1}^h N_j(\mu_j-\bar{y})^2}{\sigma^2} \sim \chi^2(h-1)`$
    - $`\sum(y_k-\bar{y})^2=\sum(y_k-\mu(x_k)+\mu(x_k)-\bar{y})^2=\sum(y_k-\mu(x_k))^2+\sum(\mu(x_k)-\bar{y})^2+2\sum(y_k-\mu(x_k))(\mu(x_k)-\bar{y})`$
    - 右辺第3項は下記の通り0となる
        - $`\displaystyle \sum_{k=1}^n(y_k-\mu(x_k))(\mu(x_k)-\bar{y})=\sum_{j=1}^h\sum_{k\ |\ x_k=H_j}(y_k-\mu_j)(\mu_j-\bar{y})=\sum_{j=1}^h((\mu_j-\bar{y})\sum_{k\ |\ x_k=H_j}(y_k-\mu_j))=0`$
    - よって、下記が成り立つ
        - $`\displaystyle \sum(y_k-\bar{y})^2=\sum(\mu(x_k)-\bar{y})^2+\sum(y_k-\mu(x_k))^2 = \sum_{j=1}^h\sum_{k\ |\ x_k=H_j}(\mu_j-\bar{y})^2+\sum_{j=1}^h\sum_{k\ |\ x_k=H_j}(y_k-\mu_j)^2 = \sum_{j=1}^hN_j(\mu_j-\bar{y})^2+\sum_{j=1}^h\sum_{k\ |\ x_k=H_j}(y_k-\mu_j)^2`$
    - 両辺を$`\sigma^2`$で割ると、
        - $`\displaystyle \frac{\sum(y_k-\bar{y})^2}{\sigma^2} = \frac{\sum_{j=1}^hN_j(\mu_j-\bar{y})^2}{\sigma^2}+\frac{\sum_{j=1}^h\sum_{k\ |\ x_k=H_j}(y_k-\mu_j)^2}{\sigma^2}`$
    - 上式の左辺は$`\chi^2(n-1)`$、右辺第１項は$`\chi^2(h-1)`$であるから、右辺第2項は$`\chi^2(n-h)`$に従う。
    - 以上より、下記統計量はF分布に従うので、F分布の上側（有意水準[%]）点より大きい場合、帰無仮説は棄却される。
        - $`\displaystyle \frac{n-h}{h-1}\frac{\sum_{j=1}^hN_j(\mu_j-\bar{y})^2}{\sum_{j=1}^h\sum_{k\ |\ x_k=H_j}(y_k-\mu_j)^2} \sim F(h-1,n-h)`$

### 補題

- ダミー変数$`s_1,\cdots,s_{h}`$の内、最初の$`h-1`$個を用いて、線形回帰モデルは下記となる。
    - $`y =a_0 + b_1s_{1}+\cdots+b_{h-1}s_{h-1} +\epsilon`$
    - ベクトル$`b=(b_1,\cdots,b_{h-1})^{\tau}`$、$`v=(s_1,\cdots,s_{h-1})^{\tau}`$と定義すれば、
        - $`y =a_0 + v^{\tau}b +\epsilon`$
- n回の測定より、$`y_1,\cdots,y_{n}`$、$`v_1,\cdots,v_{n}`$を得たとすると、下記が成り立つ
    - $`\frac{1}{n}\sum_k y_k = \bar{y}`$
    - $`\frac{1}{n}\sum_k v_k = (p_1,\cdots,p_{h-1})^{\tau} \equiv p`$
    - $`\frac{1}{n}\sum_n y_kv_k = (p_1\mu_1,\cdots,p_{h-1}\mu_{h-1})^{\tau} \equiv r`$
- なお、ダミー変数の性質より、下記が成り立つ。
    - $`u^{\tau}(\sum v_k v_k^{\tau})=\sum v_k^{\tau}`$
    - $`u^{\tau}p=1-p_h`$
    - $`u^{\tau}r=\mu-\mu_hp_h`$
- 最小2乗法より、最小化対象は、
    - $`g = \sum(a_0+b^{\tau}v_k-y_k)^2`$ 
    - 偏微分して
        - $`\frac{\partial g}{\partial a_0} = 2\sum(a_0+b^{\tau}v_k-y_k)=2(na_0+b^{\tau}\sum v_k-\sum y_k)`$
        - $`\frac{\partial g}{\partial b} = 2\sum v_k(a_0+b^{\tau}v_k-y_k)=2(a_0\sum v_k +(\sum v_k v_k^{\tau})b-\sum y_kv_k)`$
    - 従って、最尤推定値$`\hat{a_0},\hat{b}`$について下記が成り立つ。
        - 1) $`\hat{a_0}+p^{\tau}\hat{b}-\bar{y} = 0`$
        - 2) $`\hat{a_0}p +\frac{1}{n}(\sum v_k v_k^{\tau})\hat{b}-r = 0`$
    - 2番目の式に左から$`u^{\tau}`$をかけると、
        - 3) $`\hat{a_0}(1-p_h) +p^{\tau}\hat{b}-(\bar{y}-\mu_hp_h) = 0`$
    - 従って式1から式3を引くと
        - $`\hat{a_0}p_h- \mu_hp_h= 0`$
        - 即ち
            - $`\hat{a_0} = \mu_h`$
    - 式2は、下記となる。
        - $`\mu_h\left(\begin{array}{c} p_1 \\ \vdots \\ p_{h-1} \end{array}\right)+\left(\begin{array}{c} p_1\hat{b}_1 \\ \vdots \\ p_{h-1}\hat{b}_{h-1} \end{array}\right)-\left(\begin{array}{c} p_1\mu_1 \\ \vdots \\ p_{h-1}\mu_{h-1} \end{array}\right)=0`$
    - 従って、
        - $`\hat{b}=\left(\begin{array}{c} \hat{b}_1 \\ \vdots \\ \hat{b}_{h-1} \end{array}\right) = \left(\begin{array}{c} \mu_1-\mu_h \\ \vdots \\ \mu_{h-1}-\mu_h \end{array}\right)`$
- よって、yの推定値$`\hat{y}`$を求める式は
    - $`\hat{y} = \mu_h + (\mu_1-\mu_h)s_{1}+\cdots+(\mu_{h-1}-\mu_h)s_{h-1}`$
- さらに、$`1-(s_1+\cdots+s_{h-1})=s_h`$を用いれば、式を変形でき
    - $`\hat{y} = \mu_1s_{1}+\cdots+\mu_{h}s_{h}`$