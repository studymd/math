# 定義

## 尤度比検定（likelihood ratio test）とは
尤度比Λ（の対数の-2倍）を[検定統計量](../base/statistic.md)として用いる[仮説検定](../base/testing.md)の総称。

- サイズnの[標本](../base/sample.md)が得られる尤度について考えるとき、nが十分に大きければ、検定統計量 −2 log Λ が、漸近的に[カイ二乗分布](../distribution/chi_squared.md)に近似できること（Wilks’ theorem）を利用する場合がある

---
# 統計量の構成

- パラメータベクトル$`\theta=(\theta_1,\cdots,\theta_p)^{\tau} \in \R^p`$によって定まる[確率密度関数](../base/probability_distribution.md)$`f(x;\theta)`$について、n回の独立な測定を行った結果得られるサイズnの[任意標本](../base/sample.md)（[確率変数](../base/random_variable.md)の列）を$`X_1,\cdots,X_n`$とする。
- パラメータベクトル$`\theta`$について、その定義域を集合$`{\bf \Theta_{full}} \subset \R^{p}`$とする時、$`\theta_1,\cdots,\theta_p`$に関するr個の制約条件式$`g_1(\theta)=0,\cdots,g_r(\theta)=0`$を満たす$`\theta`$の集合を$`{\bf \Theta_{limited}} \subset {\bf \Theta_{full}}`$とする

- サイズnの任意標本の[尤度関数](../base/likelihood_function.md)$`L_n(\theta)=\prod_{1 \leq k \leq n}{f(X_k;\theta)}`$に対して、
    - $`\hat{\theta}_{full}`$は、パラメータの定義域全域における[最尤推定量](mle.md)（確率変数）とする
        - $`\hat{\theta}_{full}={\displaystyle \argmax_{\theta \in \Theta_{full}}L_n(\theta)}`$
    - $`\hat{\theta}_{limited}`$は、制約条件の下でのパラメータの最尤推定量（確率変数）とする
        - $`\hat{\theta}_{limited} = {\displaystyle \argmax_{\theta \in \Theta_{limited}}L_n(\theta)}`$
    - $`\theta_{true}`$は真のパラメータ（定数）とする
- ここで$`\theta`$について、下記の検定を行う場合を考える。
    - 帰無仮説：$`\theta_{true} \in {\bf \Theta_{limited}}`$
    - 対立仮説：$`\theta_{true} \notin {\bf \Theta_{limited}}`$
- 尤度比$`\Lambda_n`$（確率変数）を下記で定める
    - $`\Lambda_n \equiv \frac{L_n(\hat{\theta}_{limited})}{L_n(\hat{\theta}_{full})}`$
- 検定統計量$`\lambda_n \geq 0`$を下記で定める
    - $`\lambda_n \equiv -2 log\Lambda_n = -2(logL_n(\hat{\theta}_{limited})-logL_n(\hat{\theta}_{full}))`$
    - 仮説が成り立つ場合に尤度比Λが1に近く、仮説が誤りの場合に0に近い値をとる確率が高いことから、検定統計量$`\lambda_n`$は、仮説が成り立つ場合に小さく、仮説が誤りの場合に大きな値をとる確率が高い。（つまり一定の値より大きい場合に帰無仮説を棄却するとの基準を設けることで仮説検定ができる）
    - n大であれば、$`\lambda_n`$は漸近的に[$`\chi^2`$分布](../distribution/chi_squared.md)に従う

# 派生する検定
尤度比検定をもとにして近似を利用した検定が数多くある

## 分割表に対する検定
下記の[分割表](../base/contingency_table.md)に対する検定は、尤度比検定をもとにして近似を利用した結果得られる

|検定名|統計量の近似有無|統計量が従う確率分布の近似有無|
|-|-|-|
|多項検定・正確確率検定|近似なし|近似なし|
|[G検定](g_test.md)|近似なし|近似あり（n大のもとで$`\chi^2`$分布と近似）|
|ピアソンの$`\chi^2`$検定|近似あり|近似あり（$`np_{i,j}>10`$のもとで$`\chi^2`$分布と近似）|

---

# 性質
ここでは、制約条件は線形である場合のみを考える

- 即ち、制約条件$`g_k(\theta)=0`$がノルムが1である法線ベクトル$`t_k \in \R^p`$と定数$`c_k \in \R`$を用いて、$`g_k(\theta)=\theta^{\tau}t_k-c_k=0`$と表現できる場合を考える
- その場合制約条件は$`T_0^{\tau}\theta-c=0`$とかける。
    - 行列$`T_0 = (t_1,\cdots,t_r) \in \R^{p \times r}`$
    - ベクトル$`c = (c_1,\cdots,c_r)^{\tau} \in \R^{r}`$
- さらに、適当な線形変換$`T`$を用いてパラメータ変換$`\theta' = T^{\tau}\theta`$を行えば制約条件は$`\theta'_1=c_1,\cdots,\theta'_r=c_r`$のように簡略化できる。

上記のように制約条件は変換できるので、以下では制約条件が下記である場合のみを考えることとする（パラメータの変換を考えれば、任意の線形な制約条件を考えていることに等しい）

- $`(\theta_1-c_1,\cdots,\theta_r-c_r)=0`$

また、対数尤度関数の１階偏微分を表すベクトル$`S_n(\theta) \in \R^{p}`$、２階偏微分を表す行列$`H_n(\theta) \in \R^{p \times p}`$を下記で定義する

- $`S_n(\theta) \equiv \frac{\partial}{\partial \theta}{logL_n(\theta)} `$
- $`H_n(\theta) \equiv \frac{\partial}{\partial \theta^{\tau}}\frac{\partial}{\partial \theta}{logL_n(\theta)}`$


## 最尤推定値の性質

上記の束縛条件の前提のもとで、束縛条件を考慮しない最尤推定値$`\hat{\theta}_{full}`$、束縛条件の下での最尤推定値$`\hat{\theta}_{limited}`$について下記が成り立つ。

1. $`S_n(\hat{\theta}_{full})=0`$
2. $`S_n(\hat{\theta}_{limited}) = \left(\begin{array}{r} u \\ 0 \\ \vdots \\ 0\end{array}\right) `$
    - $`u \in \R^r`$はラグランジュの未定乗数
    - 証明：束縛条件のもとで$`Argmax_{\theta}[log L_n(\theta)]`$を求めるラグランジュ法を用いる
        - 関数$`f(\theta)=logL_n(\theta) - (\theta_1-c_1,\cdots,\theta_r-c_r)^{\tau}u`$を定める
        - $`\frac{\partial}{\partial \theta}f(\theta)=S_n(\theta)-(u,0,\cdots,0)^{\tau}`$
        - 上式に$`\hat{\theta}_{limited}`$を代入すると0となる（ラグランジュ法）

## n大の場合に成り立つ近似
検定統計量$`\lambda_n`$は、帰無仮説が正しいと仮定するとnが十分に大きいなら近似的にカイ2乗分布$`\chi^2(r)`$に従う

- 真の値$`\theta_{true}`$での値について、下記の略記を用いる。
    - $`S'_n = S_n(\theta_{true})`$ ※確率変数
    - $`\mathcal{I'}_n=\mathcal{I}_n(\theta_{true})`$ ※非確率変数
- $`M \equiv \left(\begin{array}{r} I_r \\ O \end{array}\right)((I_r \ O)\mathcal{I'}_n^{-1}\left(\begin{array}{r} I_r \\ O \end{array}\right))^{-1}(I_r \ O)`$を定義する
    - $`M\mathcal{I'}_n^{-1}M =  M`$が成り立つ
- 証明）近似1~6は後述
    - 近似2より、
        - $`\lambda_n = -2(logL_n(\hat{\theta}_{limited})-logL_n(\hat{\theta}_{full})) \simeq (\hat{\theta}_{limited}-\hat{\theta}_{full})^{\tau}H(\hat{\theta}_{full})(\hat{\theta}_{limited}-\hat{\theta}_{full})`$
    - さらに近似3より、
        - $`\lambda_n \simeq (\hat{\theta}_{full}-\hat{\theta}_{limited})^{\tau}\mathcal{I'}_n(\hat{\theta}_{full}-\hat{\theta}_{limited})`$
    - 一方、近似4,5より
        - $`\hat{\theta}_{full}-\hat{\theta}_{limited} \simeq \mathcal{I'}_n^{-1}M\mathcal{I'}_n^{-1}S'_n`$
    - 上記を代入して、$`M\mathcal{I'}_n^{-1}M =  M`$を用いれば、
        - $`\lambda_n \simeq {S'_n}^{\tau}\mathcal{I'}_n^{-1}M\mathcal{I'}_n^{-1}S'_n`$
    - $`e \equiv \frac{1}{\sqrt{n}}\mathcal{I'}_1^{-\frac{1}{2}}S'_n = \mathcal{I'}_n^{-\frac{1}{2}}S'_n`$と置いて、式を変形すると、
        - $`\lambda_n \simeq e^{\tau}\mathcal{I'}_n^{-\frac{1}{2}}M\mathcal{I'}_n^{-\frac{1}{2}}e`$
    - $`\mathcal{I'}_n^{-\frac{1}{2}}M\mathcal{I'}_n^{-\frac{1}{2}}`$は非確率変数のベキ等行列であり、近似6より$`e \sim N(0,I)`$であるので、[正規分布](../distribution/normal.md)にて記載の通り、下記が成り立つ。
        - $`\lambda_n \sim \chi^2(rank(\mathcal{I'}_n^{-\frac{1}{2}}M\mathcal{I'}_n^{-\frac{1}{2}}))`$
    - Mのランクは、その定義より明らかにrであるので、題意が示された。
        - $`\lambda_n \sim^{n \rightarrow \infty} \chi^2(r)`$

### 証明で使用してる近似

1. $`H_n(\theta_{true})`$の[フィッシャー情報行列](../operator/fisher_information.md)（非確率変数）への近似
    - $`H_n(\theta_{true}) \simeq -n\mathcal{I'}_1 = -\mathcal{I'}_n`$
2. $`log L(\hat{\theta}_{limited})`$の$`\hat{\theta}_{full}`$周りにおける２次までの[テーラー展開](../base/differential.md)
    - $`log L_n(\hat{\theta}_{limited}) \simeq log L_n(\hat{\theta}_{full}) + (\hat{\theta}_{limited}-\hat{\theta}_{full})^{\tau}S_n(\hat{\theta}_{full})+\frac{1}{2}(\hat{\theta}_{limited}-\hat{\theta}_{full})^{\tau}H_n(\hat{\theta}_{full})(\hat{\theta}_{limited}-\hat{\theta}_{full}) = log L_n(\hat{\theta}_{full}) +\frac{1}{2}(\hat{\theta}_{limited}-\hat{\theta}_{full})^{\tau}H_n(\hat{\theta}_{full})(\hat{\theta}_{limited}-\hat{\theta}_{full})`$
3. $`H_n(\hat{\theta}_{full})`$の$`H_n(\theta_{true})`$への近似（近似１も使用）
    - $`H_n(\hat{\theta}_{full}) \simeq H_n(\theta_{true})\simeq -\mathcal{I'}_n`$
4. $`S_n(\hat{\theta}_{full})`$の$`\theta_{true}`$周りにおける1次までの[テーラー展開](../base/differential.md)（近似１も使用）
    - $`S_n(\hat{\theta}_{full}) \simeq S'_n + H_n(\theta_{true})(\hat{\theta}_{full}-\theta_{true}) = S'_n -\mathcal{I'}_n(\hat{\theta}_{full}-\theta_{true})`$
    - $`S_n(\hat{\theta}_{full})`=0$なので、上式は下記になる
    - $`\hat{\theta}'_{full}-\theta'_{true} \simeq \mathcal{I'}_n^{-1}S'_n`$
5. $`S_n(\hat{\theta}_{limited})`$の$`\theta_{true}`$周りにおける1次までの[テーラー展開](../base/differential.md)（近似１も使用）
    - $`S_n(\hat{\theta}_{limited}) \simeq S'_n + H_n(\theta_{true})(\hat{\theta}_{limited}-\theta_{true}) = S'_n -\mathcal{I'}_n(\hat{\theta}_{limited}-\theta_{true})`$
    - $`S_n(\hat{\theta}_{limited})=(u,0,\cdots,0)^{\tau}`$なので、上式は下記になる
    - $`\hat{\theta}'_{limited}-\theta'_{true} \simeq \mathcal{I'}_n^{-1}(S'_n-(u,0,\cdots,0)^{\tau})`$
    - ここで、帰無仮説のもとでは$`\hat{\theta}_{limited}-\theta_{true}`$の第１成分から第r成分までは0であることから、下記が成り立つ。
        - $`(I_r \ O)(\hat{\theta}_{limited}-\theta_{true}) = 0`$
        - $`\left(\begin{array}{r} I_r \\ O \end{array}\right)u = \left(\begin{array}{r} u \\ 0 \\ \vdots \\ 0\end{array}\right)`$
    - 従って、uを下記の通り求められる。
        - $`(I_r \ O)(\hat{\theta}'_{limited}-\theta'_{true}) \simeq (I_r \ O)\mathcal{I'}_n^{-1}(S'_n-\left(\begin{array}{r} u \\ 0 \\ \vdots \\ 0\end{array}\right))) = (I_r \ O)\mathcal{I'}_n^{-1}S'_n- (I_r \ O)\mathcal{I'}_n^{-1}\left(\begin{array}{r} I_r \\ O \end{array}\right)u = 0`$
        - $`u= ((I_r \ O)\mathcal{I'}_n^{-1}\left(\begin{array}{r} I_r \\ O \end{array}\right))^{-1}(I_r \ O)\mathcal{I'}_n^{-1}S'_n`$
        - $`\left(\begin{array}{r} u \\ 0 \\ \vdots \\ 0\end{array}\right) = M\mathcal{I'}_n^{-1}S'_n`$
    - 最終的に、下記が求まる。
        - $`\hat{\theta}_{limited}-\theta_{true} \simeq \mathcal{I'}_n^{-1}(S'_n- M\mathcal{I'}_n^{-1}S'_n)`$
6. n大の場合の[中心極限定理](../base/central_limit_theorem.md)より、（証明は[フィッシャー情報量](../operator/fisher_information.md)参照）
    - $`\frac{1}{\sqrt{n}}S'_n \sim N(0,\mathcal{I'}_1)`$
    - $`\frac{1}{\sqrt{n}}\mathcal{I'}_1^{-\frac{1}{2}}S'_n \sim N(0,I)`$

---

# 例

## 母分布が正規分布の場合において分散がある値かどうかの検定
標本が正規分布に従う$`X_k \sim N(\mu,\sigma^2)`$場合

- $`\displaystyle L_n(\mu,\sigma^2)=\prod_{1 \leq k \leq n}\frac{1}{\sqrt{2\pi\sigma^2}}exp(-\frac{(X_k-\mu)^2}{2\sigma^2})=\frac{1}{(2\pi\sigma^2)^{n/2}}exp(-\frac{\sum(X_k-\mu)^2}{2\sigma^2})`$であるから、最尤推定量は下記となる。
    - $`\hat{\mu} = \frac{1}{n}\sum X_k \equiv \bar{X}`$
    - $`\hat{\sigma^2}=\frac{1}{n}\sum(X_k-\bar{X})^2`$
- 帰無仮説$`\sigma^2=\sigma^2_0`$の下での束縛条件あり最尤推定量は
    - $`\check{\mu} = \frac{1}{n}\sum X_k \equiv \bar{X}`$
- 尤度比は、
    - $`\Lambda_n = (\frac{\hat{\sigma^2}}{\sigma^2_0})^{n/2}exp(n/2)(1-\frac{\hat{\sigma^2}}{\sigma^2_0})`$
- 対数尤度比は、
    - $`\lambda_n = -n(log\hat{\sigma^2}-log\sigma^2_0)-n(1-\frac{\hat{\sigma^2}}{\sigma^2_0})`$

### 近似
- $`log\hat{\sigma^2}`$を$`log\sigma^2_0`$の周りで2次までテイラー展開すると
    - $`log\hat{\sigma^2}=log\sigma^2_0+\frac{1}{\sigma^2_0}(\hat{\sigma^2}-\sigma^2_0)-\frac{1}{2\sigma^4_0}(\hat{\sigma^2}-\sigma^2_0)^2`$
- よって対数尤度比は、
    - $`\lambda_n = \frac{n}{2}(\frac{\hat{\sigma^2}}{\sigma^2_0}-1)^2`$