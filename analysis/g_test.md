# 定義

## G検定（G test）とは
[分割表](../base/contingency_table.md)もしくは度数分布表に対する[仮説検定](../base/testing.md)の１手法であって、帰無仮説が正しいと仮定した場合に[統計量](../base/statistic.md)が近似的に[$`\chi^2`$分布](../distribution/chi_squared.md)に従うことを利用する検定の総称。同じく$`\chi^2`$分布を用いる
ピアソンの$`\chi^2`$検定より正確な方法である。ノンパラメトリック手法であり[尤度比検定](likelihood_ratio_test.md)の一種である。具体的には下記の検定が存在する。

- 適合度検定
    - 分割表（2変量度数分布）もしくは度数分布表（1変量度数分布）の度数が理論度数と等しいか検定する
- 独立性検定
    - 分割表（2変量度数分布）を構成する2変量が独立かを検定する

G検定では、尤度比検定の統計量を用いて統計値を計算し、nが十分に大きければ統計量が$`\chi^2`$分布（「独立な束縛条件」の個数を自由度とする）に従うと近似できることを利用して検定を行う。

---

# 性質

## 仮定条件と正確性
G検定は対数尤度を近似せずに用いる方法であり、ピアソンの$`\chi^2`$検定より正確である（ピアソンの$`\chi^2`$検定は、統計量を近似することで統計値をより簡単に計算できる形にしたもの）

- 標本サイズ（＝度数合計）が大きい場合
    - G検定を用いる。
    - ※小さい度数がない（適合度検定では全てのセルで小さい理論度数がない・独立性検定では全てのセルで小さい度数がない）場合はピアソンの$`\chi^2`$検定を用いることもできる（ただし、現在は計算にコンピュータが使用できるので積極的に用いる理由はない）
- 標本サイズ（＝度数合計）が小さい場合
    - G検定を使うべきではない。統計量が$`\chi^2`$分布に従わなくなるt為（[尤度比検定](likelihood_ratio_test.md)参照）
    - 代わりに適合性検定では多項検定、独立性検定ではフィッシャーの正確検定を使用する

---

# 使用方法

## 対象とする標本

ここでは、2変量$`X,Y`$に対する任意標本$`(X_1,Y_1),\cdots,(X_n,Y_n)`$に対する分割表を対象としてG検定を行う。（G検定は3次元以上でも対象にできるが、紙面に表として記載できない為、利用されることは少ない）

$`r \times s`$分割表を下記とする

- $`M=\left(\begin{array}{ccc}M_{1,1} & \cdots & M_{1,s} \\ \vdots & \ddots & \vdots \\ M_{r,1} & \cdots & M_{r,s} \end{array}\right)`$

標本が復元抽出で得たものである場合、母比率を$`p_{i,j}`$とすると

- $`M_{i,j} \sim Bi(n, p_{i,j})`$

また、$`M_{i,j}`$の期待値（即ち母平均）を$`\mu_{i,j}`$とすると

- $`\mu_{i,j} = np_{i,j}`$

## G検定を用いた適合度検定

- 母比率$`p_{i,j}`$が特定の値$`c_{i,j}`$であることを帰無仮説とする。これは即ちパラメータに対して$`rs-1`$個の「独立な束縛条件」を課したこととなる

|束縛条件|パラメータの自由度|最尤推定量|
|-|-|-|
|あり（$`p_{i,j}=c_{i,j}`$）|0|$`{\hat{p}_{i,j}}_{limited}=c_{i,j}`$<br />$`{\hat{\mu}_{i,j}}_{limited}=nc_{i,j}`$|
|なし|rs-1|$`{\hat{p}_{i,j}}_{full}=\frac{1}{n}M_{i,j}`$<br />$`{\hat{\mu}_{i,j}}_{full}=M_{i,j}`$|

- ここで[尤度比検定](likelihood_ratio_test.md)を用いれば、統計量は下記となる
    - $`\displaystyle \lambda = -2log\frac{\prod (c_{i,j})^{M_{i,j}}}{\prod ({\frac{M_{i,j}}{n}})^{M_{i,j}}}= 2\sum_{\forall i,\forall j}{M_{i,j}log\frac{M_{i,j}}{nc_{i,j}}}`$
- n大の時、下記が近似的に成り立つ
    - $`\lambda \sim \chi^2(rs-1)`$

## G検定を用いた独立性検定

- $`X_k`$と$`Y_k`$が独立であることを帰無仮説とする。即ち、
    - $`\forall i,j \ p_{i,j} = \sum_j p_{i,j} \sum_i p_{i,j}`$
- これは即ちパラメータに対して$`(r-1)(s-1)`$個の「独立な束縛条件」を課したこととなる

|束縛条件|パラメータの自由度|最尤推定量|
|-|-|-|
|あり（$`\ p_{i,j} = \sum_j p_{i,j} \sum_i p_{i,j}`$）|r+s-2|$`{\hat{p}_{i,j}}_{limited} = \frac{1}{n^2}\sum_j M_{i,j}\sum_i M_{i,j}`$<br />$`{\hat{\mu}_{i,j}}_{limited}=\frac{1}{n}\sum_j M_{i,j}\sum_i M_{i,j}`$|
|なし|rs-1|$`{\hat{p}_{i,j}}_{full}=\frac{1}{n}M_{i,j}`$<br />$`{\hat{\mu}_{i,j}}_{full}=M_{i,j}`$|

- ここで[尤度比検定](likelihood_ratio_test.md)を用いれば、統計量は下記となる。
    - $`\lambda = -2log\frac{\prod (\frac{1}{n^2}\sum_j M_{i,j}\sum_i M_{i,j})^{M_{i,j}}}{\prod ({\frac{M_{i,j}}{n}})^{M_{i,j}}}= 2\sum_{\forall i,\forall j}{M_{i,j}log\frac{M_{i,j}}{\frac{1}{n}\sum_j M_{i,j}\sum_i M_{i,j}}}`$
- n大の時、下記が近似的に成り立つ
    - $`\lambda \sim \chi^2((r-1)(s-1))`$

## 統計量の形
統計値は適合度検定でも独立性検定でも下記の形になる

- $`\displaystyle \lambda = 2\sum_{\forall i,\forall j}{{\hat{\mu}_{i,j}}_{full}log\frac{{\hat{\mu}_{i,j}}_{full}}{{\hat{\mu}_{i,j}}_{limited}}}`$

### 近似

以下、簡単化の為に束縛条件なしの推定量を$`z_{i,j}={\hat{\mu}_{i,j}}_{full}`$、束縛条件ありの推定量を$`d_{i,j}={\hat{\mu}_{i,j}}_{limited}`$とかく。

- $`z_{i,j}`$の$`d_{i,j}`$周りでの２次テーラー展開を用いると下記が成り立つ
    - $`z_{i,j} log \frac{z_{i,j}}{d_{i,j}}=(z_{i,j}-d_{i,j})+\frac{(z_{i,j}-d_{i,j})^2}{2d_{i,j}}`$
- 一方で
    - $`\Sigma z_{i,j}=n`$と$`\Sigma d_{i,j}=n`$より$`\Sigma (z_{i,j}-d_{i,j})=0`$
- 以上を用いると下記の近似が成り立つ
    - $`\displaystyle 2\Sigma z_{i,j} log \frac{z_{i,j}}{d_{i,j}}=\Sigma\frac{(z_{i,j}-d_{i,j})^2}{d_{i,j}}`$

よって、束縛条件ありでの推定量と束縛条件なしでの推定量が十分に近い場合は

- $`\displaystyle \lambda \simeq \Sigma\frac{({\hat{\mu}_{i,j}}_{full}-{\hat{\mu}_{i,j}}_{limited})^2}{{\hat{\mu}_{i,j}}_{limited}}`$

この近似形を用いるのがピアソンの$`\chi^2`$検定である

## 統計値の計算

統計量において$`M_{i,j}`$に分割表の実現値（＝観測度数）$`m_{i,j}`$を代入することで統計値$`\lambda_{data}`$が得られる。統計値は適合度検定でも独立性検定でも、期待度数$`E_{i,j}`$という考えを導入すれば同じ形になる（期待度数とは度数が従う確率分布の平均の推定量$`{\hat{\mu}_{i,j}}_{limited}`$に標本データの値を代入して得られる値のこと）

- $`\lambda_{data} = 2\sum_{\forall i,\forall j}{m_{i,j}}log \frac{m_{i,j}}{E_{i,j}}`$

- 適合度検定の場合
    - $`E_{i,j} = nc_{i,j}`$
- 独立性検定の場合
    - $`E_{i,j} = \frac{1}{n}\sum_j m_{i,j}\sum_i m_{i,j}`$