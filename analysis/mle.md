# 定義

## 最尤推定（MLE:maximum likelihood estimation）とは
推計統計における[推定](../base/estimation.md)手法（与えられたデータからそれが従う確率分布の母数を点推定する方法）の一つ。

最尤推定を用いると、[母分布](../base/probubility_distribution.md)の形を仮定（母数は変数とする）しさえすれば、母数に対する[推定量]((../base/statistic.md))（任意標本点を表す確率変数を項とする数式である確率変数）を得ることができる。また[標本データ](../base/sample.md)から計算して推定値が得られる。

母数を$`\theta`$とする分布に従う任意標本の[尤度関数](../base/likelihood_function.md)$`L_{X}(\theta;x_1,\cdots,x_n)`$を用いて下記と推定する

- 最尤推定量（確率変数）：
    - $`\displaystyle \hat{\theta}_X=\argmax_{\theta} log L_{X}(\theta;X_1,\cdots,X_n)`$
- 最尤推定値（定数）：
    - $`\displaystyle \hat{\theta}_x=\argmax_{\theta} log L_{x}(\theta)`$

機械学習ではニューラルネットワーク（特に生成モデル）を学習する際に最尤推定が用いられる

---

# 性質

- 最尤推定量は不偏推定量とは限らない
    - 正規分布の母分散の最尤推定量（下記例参照）は$`\hat{\sigma^2}=\frac{1}{n}\sum(X_k-\bar{X})^2`$であって、これは不偏分散ではない

## nが十分に大きい場合の性質

最尤推定量は一致性と漸近有効性を満たす。即ちnが十分に大きければ、下記が成り立つ。

- $`E[\hat{\theta_X}]=\theta_{true}`$
- $`V[\hat{\theta_X}]=\mathcal{I}_{X \times n}^{-1}(\theta_{true})`$
    - $`\mathcal{I}_{X \times n}(\theta_{true})`$は、真のパラメータを$`\theta_{true}`$とした時の真のフィッシャー情報量
---
# KL情報量・交差情報量（クロスエントロピー）との関係
経験分布（標本データの頻度そのものから得る確率分布）のパラメトリックモデルに対する[KL情報量](../operator/kl_information)もしくは[交差情報量](../operator/information)（クロスエントロピー）を最小化することは最尤推定することに等しい。詳しくは[KL情報量](../operator/kl_information)を参照せよ。

# 最小２乗法との関係
識別モデルでノイズを正規分布と仮定した場合の最尤推定は最小２乗法と一致する。

- 識別モデルとは
    - 入力xに対して出力yが決まるという関係を考える時に、出力yは何らかのノイズによって確率的に散らばると考えそれを確率変数Zで表すとし、$`Y=f(x;\theta)+Z`$とモデル化する
    - 識別モデルでは入力は非確率変数として扱う
- ノイズZは正規分布$`Z \sim N(0,\sigma^2)`$（$`\sigma^2`$は固定値とする）であると仮定する場合
    - $`Y \sim N(f(x;\theta),\sigma^2)`$
    - $`\theta`$の最尤推定値は最小２乗法で求められる値と等しくなる
- 尤度関数：
    - $`\displaystyle L(\theta;Y_1,\cdots,Y_N)=\prod_k exp(-\frac{(Y_k-f(x_k;\theta))^2}{\sqrt{2\pi\sigma^2}})`$
- 最尤推定量（確率変数）：
    - $`\displaystyle \hat{\theta}_Y= \argmax_{\theta} log L(\theta;Y_1,\cdots,Y_N)=\argmax_{\theta} \sum_k (-\frac{(Y_k-f(x_k;\theta))^2}{\sqrt{2\pi\sigma^2}})=\argmin_{\theta} \sum_k(Y_k-f(x_k;\theta))^2`$
- 最尤推定値（定数）：
    - $`\displaystyle \hat{\theta}_{y}= \argmin_{\theta} \sum_k(y_k-f(x_k;\theta))^2`$
    - これは最小２乗法と同じである

---
# 例

## 正規分布におけるパラメータの最尤推定

- $`N(\mu,\sigma^2)`$に従う独立な確率変数$`X_1,\cdots,X_n`$が得られたとき、最尤推定法によって母数$`\mu`$と$`\sigma^2`$の推定値$`\hat{\mu}`$と$`\hat{\sigma^2}`$を求める。
- 尤度関数は、$`L(\mu,\sigma^2) = \frac{1}{\sqrt{(2\pi\sigma^2)^n}}e^{-\frac{\sum(X_k-\mu)^2}{2\sigma2}}`$であるから、対数をとって、$`log(L(\mu,\sigma^2))=l(\mu,\sigma^2) = -\frac{n}{2}log(2\pi\sigma^2)-\frac{\sum(X_k-\mu)^2}{2\sigma2}`$。$`\mu,\sigma^2`$でそれぞれ偏微分すると、
    - $`\frac{\partial l}{\partial \mu} = -\frac{1}{2\sigma^2}(-2X+2\mu)=\frac{1}{\sigma^2}(\sum(X_k-\mu))`$
    - $`\frac{\partial l}{\partial \sigma^2} = -\frac{n}{2\sigma^2}+\frac{\sum(X_k-\mu)^2}{2\sigma4}`$
- 上記偏微分は、最尤推定値$`\hat{\mu},\hat{\sigma^2}`$の元で0となることを用いて、
    - $`\sum(X_k-\hat{\mu}) = 0`$
    - $`-\frac{n}{2\check{\sigma^2}}+\frac{\sum(X_k-\hat{\mu})^2}{2\hat{\sigma^2}^2} = 0`$
- 従って、
    - $`\hat{\mu} = \frac{1}{n}\sum X_k \equiv \bar{X}`$
    - $`\hat{\sigma^2}=\frac{1}{n}\sum(X_k-\bar{X})^2`$
