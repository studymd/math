# 定義

## 回帰分析（regression analysis）とは
多次元の[標本](../base/sample.md)について、その成分間の関係性（独立性・従属性）について調査を行う方法の一つ。ある成分（目的変数）が他の成分からなる式（説明変数）の結合で表せると仮定した上で、目的変数の推定値を求める式（回帰式）を得ることを、回帰分析と呼ぶ。

特に、「線形結合で表せる（線形従属性がある）」かつ「残差が[多変量正規分布](../distribution/normal.md)に従う」と仮定した場合を、[一般線形モデル](../base/general_linear_model.md)と呼び、その仮定での回帰分析を線形回帰分析と呼ぶ。モデルから係数ベクトルを求める際には、最小二乗法（各元の残差が独立かつ等分散である場合、残差平方和が最小になる係数ベクトルを、係数ベクトルの推定値とする）・もしくは一般化最小二乗法（各元の残差が独立でなかったり分散が異なると仮定する場合）を用いる。

## 狭義の回帰分析
回帰分析という場合、説明変数が量的変数であることを暗示する場合がある。なぜなら、説明変数が量的変数でない（質的変数である）場合、回帰で得られる係数ベクトルは、その質的変数の各水準内において目的変数の平均値を並べたベクトルであり、大仰に分析と呼ぶような作業をせずに（回帰分析の）結果が得られるからである。単に回帰分析という場合、狭義の回帰分析を指す場合が多い。

---
# 計算

## 線形回帰分析

[一般線形モデル](../base/general_linear_model.md)で定義の記法を用いる。標本データをモデルに当てはめた式$`y = U_{x}a+\varepsilon`$について、$`U_x`$と$`a`$は非確率変数、$`y`$を[確率変数](../base/random_variable.md)、$`\varepsilon`$を$`y`$に完全従属である（$`y`$が定まれば一意に定まる）確率変数として扱う。

### 最尤推定（係数ベクトルと残差ベクトルの推定）

- 最尤推定法によって、$`\varepsilon \sim N(0,\sigma^2I)`$のパラメータ$`\sigma^2`$の推定値$`\check{\sigma^2}`$、及びaの推定値$`\check{a}`$を求める。
- 尤度関数は、$`L(\sigma^2,a) = \frac{1}{\sqrt{(2\pi\sigma^2)^n}}e^{-\frac{(y-U_{x}a)^{\tau}(y-U_{x}a)}{2\sigma2}}`$であるから、対数をとって、$`log(L(\sigma^2,a))=l(\sigma^2,a) = -\frac{n}{2}log(2\pi\sigma^2)-\frac{(y-U_{x}a)^{\tau}(y-U_{x}a)}{2\sigma2}`$。$`A,\sigma^2`$でそれぞれ偏微分すると、
    - $`\frac{\partial l}{\partial a} = -\frac{1}{2\sigma^2}\frac{\partial}{\partial a}(y^{\tau}y-2a^{\tau}U_{x}^{\tau}y+a^{\tau}U_{x}^{\tau}U_{x}a)=\frac{1}{\sigma^2}(U_{x}^{\tau}y-U_{x}^{\tau}U_{x}a)`$
    - $`\frac{\partial l}{\partial \sigma^2} = -\frac{n}{2\sigma^2}+\frac{(y-U_{x}a)^{\tau}(y-U_{x}a)}{2\sigma4}`$
- 上記偏微分は、最尤推定値$`\check{\sigma^2},\check{A}`$の元で0となることを用いて、
    - $`U_{x}^{\tau}y-U_{x}^{\tau}U_{x}\check{a} = 0`$
    - $`-\frac{n}{2\check{\sigma^2}}+\frac{(y-U_{x}\check{a})^{\tau}(y-U_{x}\check{a})}{2\check{\sigma^2}^2} = 0`$
- 従って、$`U_{x}^{\tau}U_{x}`$は正則であるものとすれば、
    - $`\check{a}=(U_{x}^{\tau}U_{x})^{-1}U_{x}^{\tau}y`$
    - $`n\check{\sigma^2}=(y-U_{x}\check{a})^{\tau}(y-U_{x}\check{a})=y^{\tau}y-y^{\tau}U_{x}(U_{x}^{\tau}U_{x})^{-1}U_{x}^{\tau}y=y^{\tau}(I-Z)y`$
        - ただし、$`Z=U_{x}(U_{x}^{\tau}U_{x})^{-1}U_{x}^{\tau}`$と置いた

### 最小2乗法との関係
上記は、最小2乗法を用いて、$`|\varepsilon|^2 = (y - U_{x}a)^{\tau}(y - U_{x}a)`$を最小にするパラメータ$`\check{a}`$を偏微分$`\frac{\partial|\varepsilon|^2}{\partial a}=0`$より求めることと同等である。

### 回帰直線・回帰平面・回帰超平面
以上の結果より、説明変数$`X_k\ |\ k=1,\cdots,p`$と目的変数Yとの間に成り立つ、確率的に尤もらしい関係（誤差を最小化する関係）として、下記の式が得られる。

- $`Y \simeq (1,X_1,...,X_p)(U_{x}^{\tau}U_{x})^{-1}U_{x}^{\tau}y`$

式が表す図形は、P+1次元空間（xがp次元ｙが1次元の合計P+1次元）の中のp次元部分空間となる（1つの制約式により1次元減少する）。P=1の場合は直線となり回帰直線と呼び、P=2の場合は平面となり回帰平面と呼び、P>=3の時は、回帰超平面と呼ぶ。

### 回帰の解釈
- 目的ベクトル$`y`$が属するn次元ベクトル空間$`R^n`$において、下記のp+1個のベクトルを基底とする(p+1)次元部分空間$`R^{p+1}`$を考える。
    - すべての成分が1のベクトル$`u \in \R^n`$。
    - 説明変数$`X_k \in \R^n\ |\ k=1,\cdots,p`$（p個）
-  上記部分空間にyを正射影したものが$`\check{y}`$になり、その部分空間の直交補空間つまり{n-(p+1)}次元部分空間$`R^{n-p-1)}`$にyを正射影したものが$`\check{\varepsilon}`$になる。
-  yを$`\check{y}`$に正射影する射影子Zは、$`\check{y} \equiv U_{x}\check{a}=U_{x}(U_{x}^{\tau}U_{x})^{-1}U_{x}^{\tau}y`$から分るように、$`Z=U_{x}(U_{x}^{\tau}U_{x})^{-1}U_{x}^{\tau}`$で定まる。

---
# 性質
上記のように求めた最尤推定値$`\check{a}`$、及びそれを用いたyの回帰値$`\check{y} \equiv U_x \check{a} = Zy`$、残差ベクトル$`\check{\varepsilon} \equiv y-\check{y}=(I-Z)y`$は確率変数となる。（Zはyから回帰値$`\check{y}`$への射影子である非確率変数）

- 射影子$`Z \equiv U_{x}(U_{x}^{\tau}U_{x})^{-1}U_{x}^{\tau}`$の性質
    1. $`\check{y} = Zy`$
    2. 対称なベキ等行列である
    3. $`ZU_{x}=U_{x}`$
    4. すべての要素が1のn次元ベクトルをu、要素がすべて1の(n,n)行列をUと表記すると下記が成り立つ
        - $`Zu=u`$
            - $`ZU_{x}=U_{x}`$の第一列から明らか
        - $`ZU=U`$、$`UZ=U`$
    5. $`tr(Z)=rank(Z)=rank(U_{x})=p+1`$
- 回帰ベクトルと回帰残差ベクトルは直交する
    - 証明
        - 射影子Zが対称ベキ等行列であることより、$`\check{y}^{\tau}\check{\varepsilon}=y^{\tau}Z^{\tau}(I-Z)y=y^{\tau}(Z-Z)y=O`$
        - よって、$`\check{y}`$と$`\check{\varepsilon}`$は直交する
- データ平均は必ず回帰超平面（部分空間）に含まれる
    - 目的変数の標本としての平均（確率変数としての平均ではない）$`\bar{y}=\Sigma_{k=1}^{n} y_k`$、および説明変数の標本としての平均$`\bar{x_i}=\Sigma_{k=1}^{n} x_{ik}`$について、$`\bar{y}=(u,\bar{x_i},\cdots,\bar{x_p})\check{a}`$が成り立つことは下記の通り。
        - $`\bar{x} = (u,\bar{x_i},\cdots,\bar{x_p})^{\tau}`$と定義する。
        - $`\bar{x} = \frac{1}{n}U_{x}^{\tau}u`$及び$`\bar{y} = \frac{1}{n}y^{\tau}u`$とかける。
        - 上記を用いれば、下記が成り立つ。
            - $`\bar{y}-\bar{x}^{\tau}\check{a} = \frac{1}{n}y^{\tau}u-\frac{1}{n}u^{\tau}U_{x}(U_{x}^{\tau}U_{x})^{-1}U_{x}^{\tau}y = \frac{1}{n}(y^{\tau}u-u^{\tau}Zy)= \frac{1}{n}(y^{\tau}u-u^{\tau}y)=O`$

## 確率変数の期待値と分散

- $`\check{a}`$は、[最良不偏線形推定量](../base/estimation.md)である。
- $`\sigma^2`$は不偏ではない。不偏推定量は$`\check{\sigma^2_0}=\frac{1}{n-p-1}y^{\tau}(I-Z)y`$である。

|確率変数|期待値:$`E[\cdot]`$|分散:$`V[\cdot]`$|
|-|-|-|
|$`\varepsilon`$|0|$`\sigma^2I`$|
|$`y`$|$`U_{x}a`$|$`\sigma^2I`$|
|$`\check{a}`$|$`a`$|$`\sigma^2(U_{x}^{\tau}U_{x})^{-1}`$|
|$`\check{y}`$|$`U_{x}a`$|$`\sigma^2Z`$|
|$`\check{\varepsilon}`$|0|$`\sigma^2(I-Z)`$|
|$`\frac{\|\check{\varepsilon}\|^2}{n}=\check{\sigma^2}`$|$`\sigma^2\frac{n-p-1}{n}`$|$`2\sigma^2\frac{n-p-1}{n}`$|

- 証明
    - $`\check{a}`$の期待値
        - $`E[\check{a}]=E[(U_{x}^{\tau}U_{x})^{-1}U_{x}^{\tau}y]=E[(U_{x}^{\tau}U_{x})^{-1}U_{x}^{\tau}(U_{x}a+\varepsilon)]=(U_{x}^{\tau}U_{x})^{-1}U_{x}^{\tau}(U_{x}a+E[\varepsilon])=(U_{x}^{\tau}U_{x})^{-1}U_{x}^{\tau}U_{x}a=a`$
        - よって、不偏推定量である
    - $`\check{a}`$の分散（分散共分散行列）
        - $`V[\check{a}]=V[(U_{x}^{\tau}U_{x})^{-1}U_{x}^{\tau}y]=V[(U_{x}^{\tau}U_{x})^{-1}U_{x}^{\tau}(U_{x}a+\varepsilon)]=V[(U_{x}^{\tau}U_{x})^{-1}U_{x}^{\tau}\varepsilon]=(U_{x}^{\tau}U_{x})^{-1}U_{x}^{\tau}V[\varepsilon]U_{x}(U_{x}^{\tau}U_{x})^{-1}=\sigma^2(U_{x}^{\tau}U_{x})^{-1}`$
        - これは、フィッシャー情報量$`\mathcal{I}_{y|a}=\frac{1}{\sigma^2}U_{x}^{\tau}U_{x}`$の逆行列となっているので、クラメールラオの下限に等しく、すなわち有効推定量となっている。
    - $`\check{y}`$の期待値
        - $`E[\check{y}] = E[U_{x}\check{a}]=U_{x}E[\check{a}]=U_{x}a`$
    - $`\check{y}`$の分散（分散共分散行列）
        - $`V[\check{y}] = V[U_{x}\check{a}]=U_{x}V[\check{a}]U_{x}^{\tau}=\sigma^2U_{x}(U_{x}^{\tau}U_{x})^{-1}U_{x}^{\tau}`$
    - $`\check{\sigma^2}`$の期待値
        - $`E[\check{\sigma^2}] = E[\frac{1}{n}y^{\tau}(I-Z)y]=\frac{1}{n}E[(a^{\tau}U_{x}^{\tau}+\varepsilon^{\tau})(I-Z)(U_{x}a+\varepsilon)]=\frac{1}{n}(a^{\tau}U_{x}^{\tau}(I-Z)U_{x}a+E[\varepsilon^{\tau}\varepsilon]-E[\varepsilon^{\tau}Z\varepsilon])=\frac{1}{n}(O+tr[\sigma^2I]-tr[Z\sigma^2I])=\frac{1}{n}(n\sigma^2-(p+1)\sigma^2)=\frac{n-p-1}{n}\sigma^2`$

---
# 重相関係数

- 回帰の当てはまりの良さを表す重相関係数Rは、下記の２つのデータ間の[相関係数](../operator/correlation_coefficient.md)と定義する。(0から1の値をとる)
    - $`\check{y}`$の成分を並べたサイズnのデータ
    - yの成分を並べたサイズnのデータ
- yの偏差2乗和$`S_{y}`$,$`\check{y}`$の偏差2乗和$`S_{\check{y}}`$、yと$`\check{y}`$の偏差積和$`S_{y\check{y}}`$を用いて、下記で定義できる。
    - $`R = \frac{S_{y\check{y}}}{\sqrt{S_{y}}\sqrt{S_{\check{y}}}}`$
- 重相関係数Rの計算
    - まず、偏差ベクトル$`y_d,\check{y_d}`$を下記で定義する。
        - $`\check{y_d} = \check{y}-\frac{1}{n}U\check{y}=(I-\frac{1}{n}U)Zy`$
        - $`y_d = y-\frac{1}{n}Uy = (I-\frac{1}{n}U)y`$
    - 偏差積和・偏差自乗和は、偏差ベクトルの内積で計算できる。$`(I-\frac{1}{n}U)(I-\frac{1}{n}U)=I-\frac{1}{n}U`$を持ちいて計算すると、
        - $`S_{y\check{y}} = \check{y_d}^{\tau}y_d = y^{\tau}Z(I-\frac{1}{n}U)y= y^{\tau}(Z-\frac{1}{n}U)y`$
        - $`S_{\check{y}} = \check{y_d}^{\tau}\check{y_d} = y^{\tau}Z(I-\frac{1}{n}U)Zy= y^{\tau}(Z-\frac{1}{n}U)y`$
        - $`S_{y} = y_d^{\tau}y_d = y^{\tau}(I-\frac{1}{n}U)y`$
    - すると、重相関係数Rは、下記で計算できる。
        - $`R = \sqrt{\frac{y^{\tau}(Z-\frac{1}{n}U)y}{y^{\tau}(I-\frac{1}{n}U)y}}`$

上記の偏差ベクトルも確率変数であり、期待値と分散は下記になる。

|確率変数|期待値:$`E[\cdot]`$|分散共分散行列:$`V[\cdot]`$|
|-|-|-|
|$`\check{y_d}`$|$`(I-\frac{1}{n}U)U_{x}a`$|$`\sigma^2(Z-\frac{1}{n}U)`$|
|$`y_d`$|$`(I-\frac{1}{n}U)U_{x}a`$|$`\sigma^2(I-\frac{1}{n}U)`$|
