# 定義

## 期待値（expectation）とは

確率変数に対する演算（operation）の一つ

- 確率変数１つを引数に取り、非確率変数を得る演算
    - 確率変数の値を、その値をとる確率の重みで平均した値。確率変数$`X`$に対して、その[分布関数](probability_distribution.md)$`f_X(x)`$を用いて、期待値を$`E[X]`$は、
        - 離散型の場合：$`E[X]=\Sigma_{\forall k}kf_X(k)`$
        - 連続型の場合：$`E[X]=\int_{-\infty}^{\infty}xf_X(x)dx`$
    - 実数をとる確率変数$`X \in \R`$のみならず、実n次元数ベクトルをとる確率変数$`X \in \R^n`$（n次元確率ベクトル）や実$`n \times m`$次元行列をとる確率変数$`X \in \R^{n \times m}`$に対しても定義できる
    - 間隔尺度・比率尺度の[確率変数](random_variable.md)に対して定義できる

また、実数をとる確率変数については、確率変数のべき乗の期待値から[モーメント](moment.md)と呼ぶ値が定義される。

---
# 性質

## 実数をとる確率変数の期待値

確率変数$`X,Y \in \R`$、スカラー値$`a \in \R`$に対して、下記が成り立つ。

1. $`E[a]=a`$
2. $`E[aX]=aE[X]`$
3. $`E[X+Y]=E[X]+E[Y]`$
4. $`X,Y`$が独立であれば、$`E[XY]=E[X]E[Y]`$

## 実n次元数ベクトルをとる確率変数の期待値

確率変数$`X,Y \in \R^n`$、スカラー値$`a \in \R`$、ベクトル$`v \in \R^n`$、行列$`M \in \R^{n\times n}`$に対して、下記が成り立つ。

1. $`E[v]=v`$
2. $`E[aX]=aE[X]`$
3. $`E[v^{\tau}X]=v^{\tau}E[X]`$
4. $`E[MX]=ME[X]`$
5. $`E[X+Y]=E[X]+E[Y]`$
6. 行列Mが対称行列であれば、$`E(X^{\tau}MX) = tr(MV[X]) + E[X]^{\tau}ME[X]`$
7. $`X,Y`$が独立であれば、$`E[X^{\tau}Y]=E[X]^{\tau}E[Y]`$

---
# 計算

独立に同じ確率分布に従うn個の確率変数$`X_k \sim D;(k=1,\cdots,n)`$を含む多項式について、下記に示す各種の期待値をDのモーメント$`\mu,\sigma^2,u_4`$を用いて表す式を導出する（$`u_4`$は4次中心モーメント）。

- 計算にあたって下記の確率変数を用いる。
    - $`\bar{X} \equiv \frac{1}{n}\Sigma{X_k}`$ ※標本平均
    - $`Y_k \equiv X_k-\mu`$
    - $`\bar{Y} \equiv \frac{1}{n}\Sigma Y_k =\bar{X}-\mu`$
    - $`\bar{Y^2} \equiv \frac{1}{n}\Sigma Y_k^2`$
    - $`W_k \equiv X_k-\bar{X} = Y_k-\bar{Y}`$
    - $`\bar{W^2} \equiv \frac{1}{n}\Sigma{W_k^2} = \bar{Y^2} -\bar{Y}^2`$ ※標本分散
- 計算を簡単に行うにあたって重要な点は、確率変数が$`Y_1,Y_2,...,Y_n`$を因数とする多項式で表されるとき、$`Y_k`$をただ１つ因数に含む項の期待地は0になるという点である。これは、添え字が異なる$`Y_k`$は互いに独立であること、及び$`E[Y_k]=0`$である性質による。
    - 例えば、$`E[Y_1^2Y_2+Y_1^2Y_2^3Y_3]=E[Y_1^2]E[Y_2]+E[Y_1^2]E[Y_2^3]E[Y_3]=E[Y_1^2]\cdot 0+E[Y_1^2]E[Y_2^3]\cdot 0=0`$。
    - 以下では、期待値をとると0になる任意の多項式を$`O`$と記載する。
- 期待値の計算
    - $`E[\bar{Y^2}] = \frac{1}{n}E[\Sigma Y_k^2] = \frac{1}{n}E[nY_i^2] = \sigma^2`$
    - $`E[\bar{Y}^2] = \frac{1}{n^2}E[(\Sigma Y_k)^2] =\frac{1}{n^2}E[nY_i^2+O] = \frac{\sigma^2}{n}`$
    - $`E[\bar{Y^2}^2] = \frac{1}{n^2}E[(\Sigma Y_k^2)^2] = \frac{1}{n^2}E[nY_i^4+(n^2-n)Y_i^2Y_j^2] = \frac{1}{n}(\mu_4+(n-1)\sigma^4)`$
    - $`E[\bar{Y^2}\bar{Y}^2]] =\frac{1}{n^3}E[\Sigma Y_k^2 \cdot (\Sigma Y_k)^2] = \frac{1}{n^3}E[(\Sigma Y_k^2)(nY_i^2 + (n^2-n)Y_iY_j)] = \frac{1}{n^3}E[nY_i^4+(n^2-n)Y_i^2Y_j^2+O]=\frac{1}{n^2}(\mu_4+(n-1)\sigma^4)`$
    - $`E[\bar{Y}^4] = \frac{1}{n^4}E[(\Sigma Y_k)^4] =\frac{1}{n^4}E[nY_i^4+3n(n-1)Y_i^2Y_j^2+O] = \frac{1}{n^3}(\mu_4+3(n-1)\sigma^4)`$
    - $`E[\bar{W^2}] =E[\bar{Y^2} -\bar{Y}^2] = \sigma^2 - \frac{1}{n}\sigma^2 = \frac{n-1}{n}\sigma^2`$
    - $`E[\bar{W^2}^2]=E[\bar{Y^2}^2 -2\bar{Y^2}\bar{Y}^2+\bar{Y}^4] = \frac{1}{n}(\mu_4+(n-1)\sigma^4) -\frac{2}{n^2}(\mu_4+(n-1)\sigma^4) + \frac{1}{n^3}(\mu_4+3(n-1)\sigma^4) = \frac{n-1}{n^3}((n-1)\mu_4+(n^2-2n+3)\sigma^4)`$
- 従って、上記を用いると不偏分散$`v_{X}^2`$の平均と分散は、下記となる。
    - $`v_{X}^2 \equiv \frac{1}{n-1}\Sigma(X_k - \bar{X})^2 = \frac{n}{n-1}\bar{W^2}`$
    - $`E[v_{X}^2] =\frac{n}{n-1}E[\bar{W^2}]=\sigma^2`$
    - $`V[v_{X}^2] = E[(v_{X}^2)^2] - E[v_{X}^2]^2 = \frac{n^2}{(n-1)^2}E[\bar{W^2}^2] - \sigma^4=\frac{1}{n}\mu_4-\frac{n-3}{n(n-1)}\sigma^4`$

|X|名称|E[X]|V[X]|
|-|-|-|-|
|$`X_i`$||$`\mu`$|$`\sigma^2`$|
|$`\bar{X}`$|標本平均|$`\mu`$|$`\frac{\sigma^2}{n}`$|
|$`\bar{X}^2`$||$`\mu^2+\frac{\sigma^2}{n}`$|(no answer)|
|$`X_i^2`$||$`\mu^2+\sigma^2`$|$`\mu_4 +4\mu\mu_3+4\mu^2\sigma^2 -\sigma^4`$|
|$`\bar{X_i^2}`$||$`\mu^2+\sigma^2`$|$`\frac{\mu_4 +4\mu\mu_3+4\mu^2\sigma^2 -\sigma^4}{n}`$|
|$`Y_i`$||$`0`$|$`\sigma^2`$|
|$`\bar{Y}`$||$`0`$|$`\frac{\sigma^2}{n}`$|
|$`\bar{Y}^2`$||$`\frac{\sigma^2}{n}`$|(no answer)|
|$`Y_i^2`$||$`\sigma^2`$|$`\mu_4-\sigma^4`$|
|$`\bar{Y^2}`$||$`\sigma^2`$|$`\frac{\mu_4-\sigma^4}{n}`$|
|$`W_i`$||$`0`$|$`\frac{n-1}{n}\sigma^2`$|
|$`W_i^2`$||$`\frac{n-1}{n}\sigma^2`$|(no answer)|
|$`\bar{W^2}`$|標本分散$`s_{X}^2`$|$`\frac{n-1}{n}\sigma^2`$|$`\frac{(n-1)^2}{n^3}\mu_4-\frac{(n-3)(n-1)}{n^3}\sigma^4`$|
|$`\frac{n}{n-1}\bar{W^2}`$|不偏標本分散$`v_{X}^2`$|$`\sigma^2`$|$`\frac{1}{n}\mu_4-\frac{n-3}{n(n-1)}\sigma^4`$|

## 正規分布の場合
独立に同じ確率分布に従うn個の確率変数$`X_k \sim N(\mu,\sigma^2);(k=1,\cdots,n)`$を含む多項式について、下記に示す各種の期待値は下記になる。正規分布のモーメント$`u_3=0, u_4=3\sigma^4`$であることを用いる。

|X|E[X]|V[X]|分布|
|-|-|-|-|
|$`X_i`$|$`\mu`$|$`\sigma^2`$|正規分布|
|$`\bar{X}`$|$`\mu`$|$`\frac{\sigma^2}{n}`$|正規分布|
|$`\bar{X}^2`$|$`\mu^2+\frac{\sigma^2}{n}`$|$`\frac{4\mu^2\sigma^2}{n} +\frac{2\sigma^4}{n^2}`$|
|$`X_i^2`$|$`\mu^2+\sigma^2`$|$`4\mu^2\sigma^2 +2\sigma^4`$|
|$`\bar{X_i^2}`$|$`\mu^2+\sigma^2`$|$`\frac{4\mu^2\sigma^2 +2\sigma^4}{n}`$|
|$`Y_i`$|$`0`$|$`\sigma^2`$|正規分布|
|$`\bar{Y}`$|$`0`$|$`\frac{\sigma^2}{n}`$|正規分布|
|$`\bar{Y}^2`$|$`\frac{\sigma^2}{n}`$|$`\frac{2\sigma^4}{n^2}`$|$`W_1(1,\frac{\sigma^2}{n})`$|
|$`\frac{n}{\sigma^2}\bar{Y}^2`$|1|2|$`\chi^2(1)`$|
|$`Y_i^2`$|$`\sigma^2`$|$`2\sigma^4`$|$`W_1(1,\sigma^2)`$|
|$`\frac{1}{\sigma^2}Y_i^2`$|1|2|$`\chi^2(1)`$|
|$`\bar{Y^2}`$|$`\sigma^2`$|$`\frac{2\sigma^4}{n}`$|$`W_1(n,\frac{\sigma^2}{n})`$|
|$`\frac{n}{\sigma^2}\bar{Y^2}`$|n|2n|$`\chi^2(n)`$|
|$`W_i`$|$`0`$|$`\frac{n-1}{n}\sigma^2`$|正規分布|
|$`W_i^2`$|$`\frac{n-1}{n}\sigma^2`$|$`2\frac{(n-1)^2}{n^2}\sigma^4`$|$`W_1(1,\frac{\sigma^2(n-1)}{n})`$|
|$`\frac{n}{(n-1)\sigma^2}W_i^2`$|1|2|$`\chi^2(1)`$|
|$`\bar{W^2}`$|$`\frac{n-1}{n}\sigma^2`$|$`\frac{2(n-1)}{n^2}\sigma^4`$|$`W_1(n-1,\frac{\sigma^2}{n})`$|
|$`\frac{n}{n-1}\bar{W^2}`$|$`\sigma^2`$|$`\frac{2}{n-1}\sigma^4`$|$`W_1(n-1,\frac{\sigma^2}{n-1})`$|
|$`\frac{n}{\sigma^2}\bar{W^2}`$|n-1|2(n-1)|$`\chi^2(n-1)`$|