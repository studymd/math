# 定義

## フィッシャー情報量（fisher information）とは

1つの確率分布に対して定義される値である。確率変数に対して定義できる他、任意標本（確率変数の列）についてもその同時確率分布を考えることで定義できる

- $`\R^n`$上の確率変数１つを引数に取り、その確率分布をもとに$`\R^{n\times n}`$上の非確率変数を得る
- $`\R^n`$上の任意標本（確率変数の列）１つを引数に取り、その同時確率分布をもとに$`\R^{n\times n}`$上の非確率変数を得る

パラメータによって決まる確率分布$`D(\theta)`$に対して、その分布に従う[確率変数](../random_variable.md)や標本がそのパラメータ推定に関して有する「情報」の量を表す

- 確率変数$`X \sim D(\theta)`$のフィッシャー情報量$`\mathcal{I}_{X}(\theta)`$
    - $`\mathcal{I}_{X}(\theta) \equiv V[S_X(\theta;X)]`$
    - $`S_X(\theta;X)`$はXの[スコア関数](../base/likelihood_function)にXを代入して得られる確率変数
- 任意標本（独立同分布確率変数の列$`X_1,...,X_n \sim D(\theta)`$）のフィッシャー情報量$`\mathcal{I}_{X \times n}(\theta)`$
    - $`\mathcal{I}_{X \times n}(\theta) \equiv V[S_X(\theta;X_1,\cdots,X_n)]= n \mathcal{I}_{X_{1}}(\theta)`$
    - 後述の加法性が成り立つ為。よって、サイズnの標本のフィッシャー情報量は標本点のフィッシャー情報量のn倍になるとのみ理解しておけば十分である。
    - よって以下$`\mathcal{I}_{X}(\theta)`$についてのみ考察する

特に、母数が多次元$`\theta \in \R^p`$である場合、フィッシャー情報量はフィッシャー情報行列と呼ばれる実対称行列$`\mathcal{I}_X(\theta) \in \R^{p \times p}`$となる。

## フィッシャー情報量の求め方

- フィッシャー情報量（情報行列）は下記で計算できる
    - $`\mathcal{I}_{X}(\theta) \equiv V[S_X(\theta;X)] = E[S_X(\theta;X)S_X(\theta;X)^{\tau}]`$
- フィッシャー情報量は、[対数尤度ヘッシアン](../base/likelihood_function.md)$`H_{X}(\theta;X)`$を用いても計算できる
    - $`\mathcal{I}_{X}(\theta) \equiv -E[H_{X}(\theta;X)]`$
    - 上式が成り立つことの証明は[尤度関数](../base/likelihood_function.md)を参照
    - なお、Xの確率密度関数が１次元のパラメータ$`\theta \in \R`$であるとき、上述の式は簡単になり下記が成り立つ。
        - $`\mathcal{I}_{X}(\theta) \equiv -E[\frac{\partial^2}{\partial\theta^2}logL(\theta;X)]`$
- 即ち、対数尤度ヘッシアンの-1倍はフィッシャー情報量の不偏推定量となる
    - $`\hat{\mathcal{I}_{X}}(\theta) \equiv -H_{X}(\theta;X)`$
- 任意標本のフィッシャー情報量について、nが大の時には大数の法則より標本データ$`d_1,\cdots,d_n`$を用いてほぼ確実な推定値が得られる
    - $`\mathcal{I}_{X \times n}(\theta) = -H_{X}(\theta;d_1,\cdots,d_n)`$


---
# 性質

## 加法性
同一のパラメータを用いる２つの確率分布$`f_1(x;\theta),f_2(x;\theta)`$に従う独立な確率変数$`X_1,X_2`$について、その同時確率分布の情報量は、加法性が成り立つ

- $`\mathcal{I}_{X_1,X_2}(\theta)=\mathcal{I}_{X_1}(\theta)+\mathcal{I}_{X_2}(\theta)`$
    - 証明：$`\mathcal{I}_{X_1,X_2}=V[\frac{\partial}{\partial\theta}log{f_1(x)f_2(x)}]=V[\frac{\partial}{\partial\theta}log{f_1(x)+\frac{\partial}{\partial\theta}logf_2(x)}]=V[\frac{\partial}{\partial\theta}log{f_1(x)]+V[\frac{\partial}{\partial\theta}logf_2(x)}]=\mathcal{I}_{X_1}+\mathcal{I}_{X_2}`$

## パラメータ変換

- 確率変数$`X`$、パラメータ$`\theta \in \R^p`$、行列$`M \in \R^{p,p}`$に対して、$`M`$が正則であれば、
    - $`\mathcal{I}_{X}(M\theta) =(M^{-1})^{\tau}\mathcal{I}_{X}(\theta)M^{-1}`$
        - 証明：$`\mathcal{I}_{X}(M\theta) = E[(\frac{\partial}{\partial (M\theta)}logf)(\frac{\partial}{\partial (M\theta)}logf)^{\tau}] = E[(\frac{\partial\theta}{\partial (M\theta)}\frac{\partial logf}{\partial \theta})(\frac{\partial\theta}{\partial (M\theta)}\frac{\partial logf}{\partial \theta})^{\tau}] = E[(M^{\tau})^{-1}\frac{\partial logf}{\partial \theta}(\frac{\partial logf}{\partial \theta})^{\tau}M^{-1}]=(M^{-1})^{\tau}\mathcal{I}_{X}(\theta)M^{-1}`$
    - 従って、$`M^{\tau}\mathcal{I}_{X}(M\theta)M = \mathcal{I}_{X}(\theta)`$
- 確率変数$`X`$、パラメータ$`\theta \in \R^n`$、行列$`M \in \R^{m,p}\ |\ m>p`$に対して、$`M^{\tau}M`$が正則であれば、
    - $`\mathcal{I}_{X}(M\theta) = M(M^{\tau}M)^{-1}\mathcal{I}_{X}(\theta)(M^{\tau}M)^{-1}M^{\tau}`$
    - 従って、$`M^{\tau}\mathcal{I}_{X}(M\theta)M = \mathcal{I}_{X}(\theta)`$

---
# 例

## 正規分布の場合

- 正規分布$`f(x) = \frac{1}{\sqrt{2\pi\sigma^2}}e^{-\frac{(x-\mu)^2}{2\sigma^2}}`$においては、
    - $`logf=-\frac{1}{2}log(2\pi\sigma^2)-\frac{(x-\mu)^2}{2\sigma^2}`$
- 従って、
    - $`E[(\frac{\partial}{\partial\mu}logf)^2]=E[\frac{(x-\mu)^2}{\sigma^4}]=\frac{1}{\sigma^2}`$
    - $`E[(\frac{\partial}{\partial\sigma^2}logf)^2]=E[(-\frac{1}{2\sigma^2}+\frac{(x-\mu)^2}{2\sigma^4})^2]=\frac{1}{2\sigma^4}`$
    - $`E[(\frac{\partial}{\partial\mu}logf)(\frac{\partial}{\partial\sigma^2}logf)]=E[\frac{x-\mu}{\sigma^2}(-\frac{1}{2\sigma^2}+\frac{(x-\mu)^2}{2\sigma^4})]=0`$

- よって、パラメータ$`\theta = \left(\begin{array}{c} \mu  \\ \sigma^2 \end{array}\right)`$に対して、
    - $`\mathcal{I}_X = \left(\begin{array}{cc} \frac{1}{\sigma^2} & 0 \\ 0 & \frac{1}{2\sigma^4} \end{array}\right)`$

## 多変量正規分布（分散固定）の場合

- n次元正規分布$`f(x) = \frac{1}{\sqrt{(2\pi)^n|\Sigma|}}exp(-\frac{(x-\mu)^{\tau}\Sigma^{-1}(x-\mu)}{2})`$においては、
    - $`logf=-\frac{1}{2}log((2\pi)^n|\Sigma|)-\frac{(x-\mu)^{\tau}\Sigma^{-1}(x-\mu)}{2}`$
- 定義通り計算すると、
    - $`E[(\frac{\partial}{\partial\mu}logf)(\frac{\partial}{\partial\mu}logf)^{\tau}]=E[\Sigma^{-1}(x-\mu)(x-\mu)^{\tau}\Sigma^{-1}]=\Sigma^{-1}E[(x-\mu)(x-\mu)^{\tau}]\Sigma^{-1}=\Sigma^{-1}\Sigma\Sigma^{-1}=\Sigma^{-1}`$
- もしくは、ヘッセ行列を用いてより簡単に、
    - $`-E[\frac{\partial^2}{\partial\mu^2}logf]=-E[-\Sigma^{-1}]=\Sigma^{-1}`$
- よって、分散を固定し$`\mu`$のみをパラメータと考えた時は、
    - $`\mathcal{I}_X = \Sigma^{-1}`$

## 多変量正規分布（独立かつ等分散）の場合

- 分散共分散行列が独立かつ等分散すなわち$`\Sigma=\sigma^2I`$である（１つのパラメータで表せる）場合、
    - $`logf=-\frac{1}{2}log((2\pi)^n\sigma^2)-\frac{(x-\mu)^{\tau}(x-\mu)}{2\sigma^2}`$
- 従って、
    - $`-E[\frac{\partial^2}{\partial\mu^2}logf]=\Sigma^{-1}=\frac{1}{\sigma^2}I`$
    - $`-E[\frac{\partial^2}{(\partial\sigma^2)^2}logf]=-E[\frac{1}{2\sigma^4}-\frac{(x-\mu)^{\tau}(x-\mu)}{\sigma^6}]=-\frac{1}{2\sigma^4}+\frac{E[(x-\mu)^{\tau}(x-\mu)]}{\sigma^6}=-\frac{1}{2\sigma^4}+\frac{n\sigma^2}{\sigma^6}=\frac{2n-1}{2\sigma^4}`$
    - $`-E[\frac{\partial}{\partial\mu}\frac{\partial}{\partial\sigma^2}logf]=-E[-\frac{x-\mu}{\sigma^4}]=0`$
- よって、パラメータ$`\theta = \left(\begin{array}{c} \mu  \\ \sigma^2 \end{array}\right)`$に対して、
    - $`\mathcal{I}_X = \left(\begin{array}{cc} \frac{1}{\sigma^2}I & 0_n \\ 0_n^{\tau} & \frac{2n-1}{2\sigma^4} \end{array}\right)`$
    - なお、$`0_n`$は成分がすべて0であるn次元ベクトル
