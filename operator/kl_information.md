# 定義

## KL情報量（Kullback-Leibler information）とは
同じ台（可測空間）上の2つの確率分布の差異を計る尺度。確率変数Xの異なる２つの確率質量関数もしくは確率密度関数$`p_{X}(x)`$、$`q_{X}(x)`$に対して下記で定義する

- $`D_{KL}(p_X||q_X)=\sum_{\forall x} p_X(x)log \frac{p_X(x)}{q_X(x)}`$

---

# 性質

## 交差情報量（クロスエントロピー）との関係

pのqに対するKL情報量は、pのqに対する[交差情報量](information.md)とpの平均情報量の差に等しい

- $`D_{KL}(p_X||q_X)=H(p_X||q_X)-H(p_X)`$


## 最尤推定との関係
標本中に値の重複がない場合において、経験分布のパラメトリックモデルに対するKL情報量を最小化することは最尤推定（対数尤度関数を最大化）することに等しい

- サイズNの標本からつくる経験分布（標本の頻度から得る確率質量関数もしくは確率密度関数）は下記となる（これは確率的に決まる関数である）
    - $`f(x;X_1,\cdots,X_N)=\frac{1}{N} \sum_n \delta_{x,X_n}`$
    - $`\delta`$はデルタ関数（Xが連続確率変数の場合はディラックのデルタ関数、離散確率変数の場合はクロネッカーのデルタ関数）
    - 標本中に値の重複がない場合、値が得られたN点において1/Nの大きさのパルスが立つ関数になる
- クロスエントロピーの計算対象は下記の確率質量関数とする
    - $`p_{X}(x)`$を任意標本$`X_1,\cdots,X_N`$から得られる経験分布$`f_{data}(x;X_1,\cdots,X_N)`$
    - $`q_{X}(x;\theta)`$をパラメータ$`\theta`$による推定確率質量関数$`f_{model}(x;\theta)`$
- KL情報量を展開すると 
    - $`D_{KL}(p_X||q_X)=\sum_{\forall x} p_X(x))log \frac{p_X(x)}{f_{model}(x;\theta)}=\sum_{\forall x} p_X(x)log p_X(x)-\sum_{\forall x} p_X(x)log f_{model}(x;\theta)`$
    - 上記の１つめの総和は$`\theta`$に依存しない
    - 上記の２つめの総和は、$`p_{X}(x)`$は経験分布であることから
        - $`\sum_{\forall x} p_X(x)log f_X(x;\theta)=\frac{1}{N}\sum_{\forall i} log f_X(X_i;\theta)`$
        - これは確率的に決まるので確率変数である
- よって、KL情報量を最小化する$`\theta`$を$`\hat{\theta}`$とすると下記が成り立つ
    - $`\displaystyle \hat{\theta} = \argmin_{\theta} D_{KL}(p_X||f_X(\theta))=\argmax_{\theta} \sum_{\forall x} p_X(x)log f_X(x;\theta)=\argmax_{\theta} \frac{1}{N}\sum_{\forall i} log f_X(X_i;\theta)=\argmax_{\theta} log L_X(X_1,\cdots,X_n;\theta)`$
    - すなわちこれは、対数尤度の最大化（＝最尤推定）である
    - ちなみにこれは、経験分布（標本データの頻度そのものから得る確率分布）$`p_{X}(x)`$のパラメトリックモデル$`q_{X}(x;\theta)`$に対する交差情報量（クロスエントロピー）$`H(p_X||q_X)`$を最小化する$`\theta`$を見つけることとも同値である