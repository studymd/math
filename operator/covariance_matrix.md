# 定義

## 分散共分散行列（covariance matrix）とは

実数ベクトル上の確率変数もしくは実数ベクトルを要素とする任意標本（確率変数の列）もしくは標本データ（定数の列）に対する演算（operation）の一つ（３つの異なる意味で用いられる）

- $`\R^n`$上の確率変数１つを引数に取り、$`\R^{n\times n}`$上の非確率変数を得る演算
- $`\R^n`$を要素とする任意標本（確率変数の列）１つを引数に取り、$`\R^{n\times n}`$上の確率変数を得る演算
- $`\R^n`$を要素とする標本データ（定数）１つを引数に取り、$`\R^{n\times n}`$上の定数を得る演算

[分散](variance.md)は実数$`\R`$上でも実数ベクトル$`\R^n`$上でも定義されるが、$`\R^n | n>=2`$の場合（2次元以上の実数ベクトル）の場合に分散は実対称行列となり、分散共分散行列と呼ぶ。定義は[分散](variance.md)を参照せよ。

---
# 性質
- 確率変数の分散共分散行列$`\Sigma_X`$も任意標本の分散共分散行列$`S_X`$も標本データの分散共分散行列$`S_x`$も行列として同様の性質を持つ為、以下代表して分散共分散行列$`\Sigma`$と表記する。
- 分散共分散行列$`\Sigma`$は、半正定値対称行列である。
    - 半正定値であるので、固有値はすべて0以上の実数
    - 対称であるので、固有ベクトルが直交し、直交行列を用いて対角化可能
        - $`\Sigma \in \R^{n\times n}`$の固有値を$`\lambda_k \geq 0\ |\ k=1,2,...,n`$、対応する単位固有ベクトルを$`v_k\ |\ k=1,2,...,n`$とする
        - $`v_i \cdot v_j=0\ |\ i \neq j`$、$`v_i \cdot v_i=1`$ととることができ、直交行列$`P = [v_1 v_2 ... v_n]`$を定義できる。
        - この直交行列を用いて、$`P^{-1}\Sigma P = \left(\begin{array}{ccccc}\lambda_1 & 0 & \cdots & 0 \\ 0 & \ddots & & \vdots \\ \vdots & & \ddots & 0 \\ 0 & \cdots & 0 & \lambda_n \end{array}\right)`$と対角化できる。この行列をAとおけば、
        - よって、$`\Sigma = PAP^{-1}`$
- 確率変数$`X \in \R^n`$、スカラー値$`a \in \R`$、ベクトル$`v \in \R^n`$、行列$`M \in \R^{n\times n}`$に対して、下記が成り立つ。
    1. $`V[X]=E[XX^{\tau}]-E[X]E[X]^{\tau}`$
    2. $`V[v]=0`$
    3. $`V[aX]=a^2V[X]`$
    4. $`V[X+v] = V[X]`$
    5. $`V[v^{\tau}X]=v^{\tau}V[X]v`$
    6. $`V[MX]=MV[X]M^{\tau}`$
- ベクトルの要素間が互いに独立$`\forall{(i,j|i\neq j)}Pr(X_i=x_i,X_j=x_j)=Pr(X_i=x_i)Pr(X_j=x_j)`$であれば、$`V[X]`$は対角行列となる。

## ベキ行列$`\Sigma^n`$

 分散共分散行列が半正定値対称行列である性質を用いると、分散共分散行列をベキ乗した行列を求めることができる。正の整数$`n`$について、ベキ行列$`\Sigma^n`$は下記の半正定値対称行列となる。

- $`\Sigma^n = (PAP^{-1})^n = PA^nP^{-1} = P \left(\begin{array}{ccccc}\lambda_1^n & 0 & \cdots & 0 \\ 0 & \ddots & & \vdots \\ \vdots & & \ddots & 0 \\ 0 & \cdots & 0 & \lambda_n^n \end{array}\right) P^{-1}`$

## 平方根行列$`\Sigma^{\frac{1}{2}}`$

また、２乗すると$`\Sigma`$になる行列（すなわち平方根行列）の中で半正定値である行列$`\Sigma^{\frac{1}{2}}`$は下記の通り一意に定まる。

- $`\Sigma^{\frac{1}{2}} =P \left(\begin{array}{ccccc}\lambda_1^{\frac{1}{2}} & 0 & \cdots & 0 \\ 0 & \ddots & & \vdots \\ \vdots & & \ddots & 0 \\ 0 & \cdots & 0 & \lambda_n^{\frac{1}{2}} \end{array}\right) P^{-1}`$

## 逆行列$`\Sigma^{-1}`$

分散共分散行列が正則$`det(\Sigma) \neq 0`$である（即ち正定値である）場合に逆行列が存在し下記で定まる。

- $`\Sigma^{-1}=P \left(\begin{array}{ccccc}\lambda_1^{-1} & 0 & \cdots & 0 \\ 0 & \ddots & & \vdots \\ \vdots & & \ddots & 0 \\ 0 & \cdots & 0 & \lambda_n^{-1} \end{array}\right) P^{-1}`$

## 逆行列の平方根行列（＝平方根行列の逆行列）$`\Sigma^{-\frac{1}{2}}`$

逆行列に対しても正定値である平方根行列$`\Sigma^{-\frac{1}{2}}`$が一意に定まり、これは分散共分散行列の平方根行列の逆行列と一致する。

- $`\Sigma^{-\frac{1}{2}} =P \left(\begin{array}{ccccc}\lambda_1^{-\frac{1}{2}} & 0 & \cdots & 0 \\ 0 & \ddots & & \vdots \\ \vdots & & \ddots & 0 \\ 0 & \cdots & 0 & \lambda_n^{-\frac{1}{2}} \end{array}\right) P^{-1}`$

## 疑似逆行列$`\Sigma^{+}`$

分散共分散行列が正則でない$`rank(\Sigma)=p`$場合（即ち0でない固有値がp個の場合）に疑似逆行列は下記で定まる。

- $`\Sigma^{+}=P \left(\begin{array}{ccccc}\lambda_1^{-1} & 0 & \cdots & 0 \\ 0 & \ddots & & \vdots \\ \vdots & & \lambda_p^{-1} & 0 \\ 0 & \cdots & 0 & 0 \end{array}\right) P^{-1}`$

## 疑似逆行列の平方根行列（＝平方根行列の疑似逆行列）$`\Sigma^{\frac{+}{2}}`$

疑似逆行列に対しても半正定値である平方根行列$`\Sigma^{\frac{+}{2}}`$が一意に定まり、これは分散共分散行列の平方根行列の疑似逆行列と一致する。

- $`\Sigma^{\frac{+}{2}}=P \left(\begin{array}{ccccc}\lambda_1^{-\frac{1}{2}} & 0 & \cdots & 0 \\ 0 & \ddots & & \vdots \\ \vdots & & \lambda_p^{-\frac{1}{2}} & 0 \\ 0 & \cdots & 0 & 0 \end{array}\right) P^{-1}`$

---
# 計算例
- $`\Sigma =\left(\begin{array}{rr} 8 & 2 \\ 2 & 5\end{array}\right)`$の場合：
    - 特性方程式$`(8-\lambda)(5-\lambda)-2\cdot2=0`$を解くと、固有値は9,4である。
    - それぞれ単位固有ベクトルを求めると、
        - 固有値9に対して、$`\frac{1}{\sqrt{5}}\left(\begin{array}{r} 2 \\ 1\end{array}\right)`$
        - 固有値4に対して、$`\frac{1}{\sqrt{5}}\left(\begin{array}{r} 1 \\ -2\end{array}\right)`$
    - 従って直交行列$`P =\frac{1}{\sqrt{5}}\left(\begin{array}{rr} 2 & 1 \\ 1 & -2\end{array}\right)`$を用いて、
        - $`\Sigma =P\left(\begin{array}{rr} 9 & 0 \\ 0 & 4\end{array}\right)P^{-1} = \frac{1}{5}\left(\begin{array}{rr} 2 & 1 \\ 1 & -2\end{array}\right)\left(\begin{array}{rr} 9 & 0 \\ 0 & 4\end{array}\right)\left(\begin{array}{rr} 2 & 1 \\ 1 & -2\end{array}\right)`$
    - よって下記が成り立つ
        - 逆行列$`\Sigma^{-1} = \frac{1}{5}\left(\begin{array}{rr} 2 & 1 \\ 1 & -2\end{array}\right)\left(\begin{array}{rr} \frac{1}{9} & 0 \\ 0 & \frac{1}{4}\end{array}\right)\left(\begin{array}{rr} 2 & 1 \\ 1 & -2\end{array}\right)=\frac{1}{36}\left(\begin{array}{rr} 5 & -2 \\ -2 & 8\end{array}\right)`$
        - 逆平方根行列$`\Sigma^{-\frac{1}{2}} = \frac{1}{5}\left(\begin{array}{rr} 2 & 1 \\ 1 & -2\end{array}\right)\left(\begin{array}{rr} \frac{1}{3} & 0 \\ 0 & \frac{1}{2}\end{array}\right)\left(\begin{array}{rr} 2 & 1 \\ 1 & -2\end{array}\right)=\frac{1}{30}\left(\begin{array}{rr} 11 & -2 \\ -2 & 14\end{array}\right)`$

---
# 関連する概念

|名前|意味|
|-|-|
|[分散](variance.md)|確率変数のばらつきの程度を表す|
|[共分散](covariance.md)|実数をとる2つの確率変数間のばらつきの程度を表す|
