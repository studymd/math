# 定義

## 分散（variance）とは
確率変数もしくは任意標本（確率変数の列）もしくは標本データ（定数の列）に対する演算（operation）の一つ（３つの異なる意味で用いられる）

- $`\R^n`$上の確率変数１つを引数に取り、$`\R^{n\times n}`$上の非確率変数を得る演算
- $`\R^n`$上の任意標本（確率変数の列）１つを引数に取り、$`\R^{n\times n}`$上の確率変数を得る演算
- $`\R^n`$上の標本データ（定数）１つを引数に取り、$`\R^{n\times n}`$上の定数を得る演算

分散は間隔尺度・比率尺度の確率変数もしくはデータのばらつきの程度を表す非負の値である。演算子として$`V`$の表記を用いることが多い。

### [確率変数](../random_variable.md)の分散（非確率変数）とは
確率変数$`X`$に対する分散は$`V[X]`$と表記する。

- 実数をとる確率変数の分散$`V:\R \rightarrow \R`$
    - 確率変数$`X\in{\R}`$に対し、その分散を$`\sigma_X^2`$と表記し下記で定義する。2次[中心モーメント](moment.md)と等しい。
        - $`\sigma_X^2 \equiv V[X] \equiv E[(X-E[X])^2]`$
- 実数ベクトルをとる確率変数の分散$`V:\R^n \rightarrow \R^{n\times n}`$
    - 確率変数$`X\in{\R^n}`$に対し、その分散を$`\Sigma_X`$と表記し[分散共分散行列](covariance_matrix.md)と呼ぶ。
        - $`\Sigma_X \equiv V[X] \equiv E[(X-E[X])(X-E[X])^{\tau}]`$

### [任意標本](../sample.md)の分散・不偏分散（確率変数）とは

- 実数をとる任意標本$`X_1,\dots,X_N \in \R`$に対して
    - 分散$`s_X^2 \in \R`$
        - 2次[中心モーメント](moment.md)と等しい
        - $`s_X^2 \equiv \frac{1}{N}\Sigma_{k=1}^{N}(X_k-\bar{X})^2`$
    - 不偏分散$`v_X^2 \in \R`$
        - $`v_X^2 \equiv \frac{1}{N-1}\Sigma_{k=1}^{N}(X_k-\bar{X})^2`$
- 実ベクトルをとる任意標本$`X_1,\dots,X_N \in \R^n`$に対して
    - 分散$`S_X \in \R^{n\times n}`$
        - [分散共分散行列](covariance_matrix.md)と呼ぶ
        - $`S_X \equiv \frac{1}{N}\Sigma_{k=1}^{N}(X_k-\bar{X})(X_k-\bar{X})^{\tau}`$
    - 分散$`V_X \in \R^{n\times n}`$
        - [不偏分散共分散行列](covariance_matrix.md)と呼ぶ
        - $`V_X \equiv \frac{1}{N-1}\Sigma_{k=1}^{N}(X_k-\bar{X})(X_k-\bar{X})^{\tau}`$

### [標本データ](../sample.md)の分散・不偏分散（定数）とは

任意標本の分散や不偏分散において各任意標本点（確率変数）を実現値（定数）で置き換えて得られる値を標本データの分散$`s_x^2 \in \R, S_x \in \R^{n\times n}`$や不偏分散$`v_x^2 \in \R, V_x \in \R^{n\times n}`$と呼ぶ。

---

# 性質

## 実数をとる確率変数の分散
確率変数$`X,Y \in \R`$、スカラー値$`a \in \R`$に対して、下記が成り立つ。

1. $`V[X] \geq 0`$
2. $`V[a] = 0`$
3. $`V[aX] = a^2V[X]`$
4. $`V[X+a] = V[X]`$
5. $`V[X+Y]=V[X]+2Cov[X,Y]+V[Y]`$

$`X,Y`$が独立であれば、下記も成り立つ。

6. $`V[X+Y]=V[X]+V[Y]`$

## 実数ベクトルをとる確率変数の分散
[分散共分散行列](covariance_matrix.md)の性質を参照。

---
# 関連する概念

|名前|意味|
|-|-|
|[標準偏差](standard_deviation.md)|分散の平方根|
|[共分散](covariance.md)|実数をとる2つの確率変数間のばらつきの程度を表す|
|[分散共分散行列](covariance_matrix.md)|実数ベクトルをとる確率変数のばらつきの程度を表す行列|