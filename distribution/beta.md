# 定義
## ベータ分布(beta distribution)とは
[連続確率分布](../probability_distribution.md)の一種。独立に一様分布 U(0,1) に従う $`\alpha+\beta -1`$ 個の確率変数を大きさの順に並べ替えたとき，小さい方からα番め（大きい方からは β番目）の確率変数Xの分布がベータ分布$`Be(\alpha,\beta)`$。

|項目|値|
|-|-|
|分布を表す記号|$`Be(\alpha,\beta)`$, $`\alpha>0`$, $`\beta > 0`$|
|定義域|$`0 \leq x \leq 1`$|
|確率密度関数|$`f(x) = Cx^{\alpha-1}(1-x)^{\beta-1}`$<br />Cは規格化定数。$`C=B(\alpha,\beta)^{-1}`$。Bはベータ関数。|
|平均|$`\frac{\alpha}{\alpha+\beta}`$|
|分散|$`\frac{\alpha\beta}{(\alpha+\beta)^2(\alpha+\beta+1)}`$|

## ベータ関数とは
$`B(\alpha,\beta) \equiv \int_0^1 x^{\alpha-1}(1-x)^{\beta-1}`$で定まる関数。正の整数m,nに対して、$`B(m,n)=\frac{(m-1)!(n-1)!}{(m+n-1)!}`$
ガンマ関数と下記の関係がある。

- $`B(\alpha,\beta)=\frac{\Gamma(\alpha)\Gamma(\beta)}{\Gamma(\alpha+\beta)}`$

---
# 性質

## 他の分布との関係

- [二項分布](binomial.md)の確率関数の部分和はベータ分布を使って計算できる。
    - $`\Sigma_{x=0}^{k}f(x;Bi(n,p)) = \int_p^1 f(x;Be(k+1,n-k))dx=\frac{\int_p^1 x^{k}(1-x)^{n-k-1}dx}{\int_0^1 x^{k}(1-x)^{n-k-1}dx}`$ 
- ベータ分布は[二項分布](binomial.md)の共役事前分布
    - $`f(x;Bi(n,\theta)f(\theta;Be(\alpha,\beta)) \propto f(x;Be(\alpha + x,\beta + n - x))`$