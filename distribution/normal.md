# 定義

## 正規分布(normal distribution)とは
[連続確率分布](../probability_distribution.md)の一種。
実数をとる確率変数が従う正規分布（１次元正規分布）を$`\mu \in \R,\sigma^2 \in \R`$を用いて$`N(\mu,\sigma^2)`$、
ｎ次元実ベクトルをとる確率変数が従う正規分布（多次元正規分布）を$`\mu \in \R^n,\Sigma \in \R^{n\times n}`$を用いて$`N(\mu,\Sigma)`$と表記する。

### １次元正規分布
|項目|値|
|-|-|
|分布を表す記号|$`N(\mu,\sigma^2)`$, $`\mu \in \R`$, $`0 < \sigma^2 \in \R`$|
|定義域|$`x \in \R`$|
|確率密度関数|$`f(x) = \frac{1}{\sqrt{2\pi\sigma^2}}e^{-\frac{(x-\mu)^2}{2\sigma^2}}`$|
|平均|$`\mu`$|
|分散|$`\sigma^2`$|

### 多次元正規分布
|項目|値|
|-|-|
|分布を表す記号|$`N(\mu,\Sigma)`$, $`\mu \in \R^n`$, $`\Sigma \in \R^{n \times n}`$|
|定義域|$`x \in \R^n`$|
|確率密度関数|$`f(x) = \frac{1}{\sqrt{(2\pi)^n\|\Sigma \|}}e^{-\frac{1}{2}(x-\mu)^{\tau}\Sigma^{-1}(x-\mu)}`$|
|平均|$`\mu`$|
|分散|$`\Sigma`$|

---
# 性質

再生性
: $`X \sim N(\mu_x, \sigma^2_x),Y \sim N(\mu_y, \sigma^2_y) \Rightarrow X+Y \sim N(\mu_x+\mu_y, \sigma^2_x+\sigma^2_y)`$

無相関なら独立
: <span style="color:orange">無相関（共分散が0）な2つの正規分布に従う確率変数は、独立である。</span>（注：一般の確率分布では、無相関であっても独立とは限らない。）
- 多次正規分布は基底を適切に変換すれば必ずベクトル要素間を無相関（共分散を0）にすることができ、従ってベクトル要素間を独立にすることができる。
- 多次元標準正規分布$`N(0,I)`$（Iは単位行列）に従う確率変数ベクトルの各要素はそれぞれ独立に標準正規分布$`N(0,1)`$に従う。

---
# 変換

## １次元正規分布
$`X \sim N(\mu,\sigma^2)`$の場合、

- $`a,b \in \R`$に対して
    - $`aX+b \sim N(a\mu+b,a^2\sigma^2)`$
    - $`\frac{X-\mu}{\sigma} \sim N(0,1)`$

## 多次元正規分布
$`X \sim N(\mu,\Sigma)`$（n次元）の場合

- $`M \in \R^{n \times n}, v \in \R`$、及び対象なベキ等行列$`A \in \R^{n \times n}`$に対して、
    - $`MX+v \sim N(M\mu+v,M\Sigma M^{\tau})`$
    - 分散共分散行列が正則である場合
        - $`\Sigma^{-\frac{1}{2}}(X-\mu) \sim N(0,I)`$
            - $`\Sigma^{-\frac{1}{2}}`$は、[分散共分散行列の逆平方根行列](../operator/covariance_matrix.md)
        - $`A\Sigma^{-\frac{1}{2}}(X-\mu) \sim N(0,A)`$
        - $`P^{-1}A\Sigma^{-\frac{1}{2}}(X-\mu) \sim N(0,\left(\begin{array}{cc} I_{rank(A)} & O \\ O & O \end{array}\right))`$
            - ただしPは、Aを対角化する直交行列：$`P^{-1}AP=\left(\begin{array}{cc} I_{rank(A)} & O \\ O & O \end{array}\right)`$
    - 分散共分散行列が正則でない場合
        - $`\Sigma^{\frac{+}{2}}(X-\mu) \sim N(0,\left(\begin{array}{cc} I_{rank(\Sigma)} & O \\ O & O \end{array}\right))`$
            - $`\Sigma^{\frac{+}{2}}`$は、[分散共分散行列の疑似逆平方根行列](../operator/covariance_matrix.md)


## 他の分布との関係

- n次元正規分布に従う$`X \sim N(\mu,\Sigma)`$について、
    - 分散共分散行列が正則である場合
        - 逆行列$latxe \Sigma^{-1}$が定義でき、2次形式$`(X-\mu)^{\tau}\Sigma^{-1}(X-\mu)`$は、自由度nの[カイ二乗分布](chi_squared.md)$`\chi^2(n)`$に従う。
            - $`(X-\mu)^{\tau}\Sigma^{-1}(X-\mu) \sim \chi^2(n)`$
        - 任意の対称なベキ等行列$`A \in \R^{n \times n}`$に対して、
            - $`(X-\mu)^{\tau}\Sigma^{-\frac{1}{2}}A\Sigma^{-\frac{1}{2}}(X-\mu) \sim \chi^2(rank(A))`$
    - 分散共分散行列が正則でない場合
        - 分散共分散行列の一般化逆行列（疑似逆行列）$`\Sigma^{+}`$による2次形式$`(X-\mu)^{\tau}\Sigma^{+}(X-\mu)`$は、自由度$`rank(\Sigma)`$のカイ二乗分布$`\chi^2(rank(\Sigma))`$に従う。
            - $`(X-\mu)^{\tau}\Sigma^{+}(X-\mu) \sim \chi^2(rank(\Sigma))`$
            - $`rank(\Sigma)`$は分散共分散行列の0でない固有値の(重解も含めた)数と等しい。

