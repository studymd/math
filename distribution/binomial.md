# 定義

## 二項分布（binomial_distribution）とは
結果が成功か失敗のいずれかであり、成功確率pが一定であるとき、n回の独立な試行を行った結果の成功数Xが従う[離散確率分布](../probability_distribution.md)

|項目|値|
|-|-|
|分布を表す記号|$`Bi(n,p)`$, $`n\in \N^{+}`$, $`0 \leq p \leq 1`$|
|定義域|$`x\in{N^{0}}, x \leq n`$|
|確率質量関数|$`f(x) = {}_{n}C_{x}p^x(1-p)^{n-x}`$|
|モーメント母関数|$`M(t)=(1-p+pe^t)^n`$|
|平均|$`np`$|
|分散|$`np(1-p)`$|

---
# 性質

- 再生性
    - $`X \sim Bi(n_x, p),Y \sim Bi(n_y, p) \Rightarrow X+Y \sim Bi(n_x+n_y,p)`$

## 他の分布との関係

二項分布はnが大きいとき、ポアソン分布もしくは[正規分布](normal.md)で近似できる。

|近似条件|分布|
|-|-|
|np(1-p)>5| $`Bi(n,p) \rightarrow N(np,np(1-p))`$|
|pが十分に小さく、nが十分に大きい|$`Bi(n,p) \rightarrow Po(np)`$|

- 二項分布の確率関数の部分和は[ベータ分布](beta.md)を使って計算できる。
    - $`\Sigma_{x=0}^{k}f(x;Bi(n,p)) = \int_p^1 f(x;Be(k+1,n-k))dx=\frac{\int_p^1 x^{k}(1-x)^{n-k-1}dx}{\int_0^1 x^{k}(1-x)^{n-k-1}dx}`$ 
- [ベータ分布](beta.md)は二項分布の共役事前分布
    - $`f(x;Bi(n,\theta)f(\theta;Be(\alpha,\beta)) \propto f(x;Be(\alpha + x,\beta + n - x))`$

二項分布に従う確率変数$`X \sim Bi(n,p)`$に対して、pが十分に小さくかつ$`np>10`$であれば、$`\frac{(X-np)}{\sqrt{np}}`$は標準正規分布に従う
