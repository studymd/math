# 定義

## 中心極限定理（central limit theorem）とは
[母分布](sample.md)がどんな分布であっても、[標本平均](sample.md)は近似的に[正規分布](../distribution/normal.md)に従うとする定理。

- 即ち標本$`X_1,\cdots,X_n`$を構成する[確率変数](random_variable.md)がそれぞれ同一の分布$`D(\mu,\sigma^2)`$に独立に従う場合、標本平均$`\bar{X}=\frac{1}{n}\Sigma{X_k}`$は、nが十分に大きければ、$`\bar{X}\sim N(\mu,\frac{\sigma^2}{n})`$となる。

---
## 標本平均の性質

- [不偏性](estimation.md)
    - 標本平均は、期待値が母平均に一致する
- [有効性](estimation.md)
    - 分散は理論上の最小（[クラメール・ラオの下限](../operator/fisher_information.md)）である$`\frac{\sigma^2}{n}`$となる。
- [一致性](estimation.md)
    - nが大の時に分散は0になる
- [漸近正規性](estimation.md)
    - nが大の時に正規分布になる
