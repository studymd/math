# 定義

## ベクトル微分とは

$`u \in \R^m,v \in \R^n`$について、ベクトルをベクトルで微分した結果を表す行列を下記で定義する。

- $`\frac{\partial}{\partial u}v^{\tau} \equiv \left(\begin{array}{ccc}  \frac{\partial v_1}{\partial u_1} & \cdots & \frac{\partial v_n}{\partial u_1} \\ \vdots & \ &  \vdots \\  \frac{\partial v_1}{\partial u_m} & \cdots & \frac{\partial v_n}{\partial u_m} \end{array}\right)`$
- $`\frac{\partial}{\partial u^{\tau}}v \equiv \left(\begin{array}{ccc}  \frac{\partial v_1}{\partial u_1} & \cdots & \frac{\partial v_1}{\partial u_m} \\ \vdots & \ &  \vdots \\  \frac{\partial v_n}{\partial u_1} & \cdots & \frac{\partial v_n}{\partial u_m} \end{array}\right)`$

## 行列微分とは

$`L \in \R,Z \in \R^{m,n}`$について、スカラー値を行列で微分した結果を表す行列を下記で定義する。

- $`\frac{\partial}{\partial Z}L \equiv \left(\begin{array}{ccc}  \frac{\partial L}{\partial Z_{1,1}} & \cdots & \frac{\partial L}{\partial Z_{1,n}} \\ \vdots & \ &  \vdots \\  \frac{\partial L}{\partial Z_{m,1}} & \cdots & \frac{\partial L}{\partial Z_{m,n}} \end{array}\right)`$

---
# ベクトル微分性質

## ベクトル微分
1. 転置
    - $`(\frac{\partial}{\partial u}v^{\tau})^{\tau}=\frac{\partial}{\partial u^{\tau}}v`$
2. 連鎖律：　任意の$`t \in \R^{l}`$について、
    - $`\frac{\partial}{\partial u}v^{\tau}=(\frac{\partial}{\partial u}t^{\tau})(\frac{\partial}{\partial t}v^{\tau})`$
3. 行列変換：　任意の$`M \in \R^{l,m}`$について、
    - $`\frac{\partial}{\partial u}(Mu)^{\tau}=M^{\tau}`$
4. 行列変換：　列が線形独立である$`M \in \R^{m,l}`$について、
    - $`\frac{\partial}{\partial (Mv)}v^{\tau}=M(M^{\tau}M)^{-1}`$

## 行列微分

- $`X \in \R^{m,l},Y \in \R^{l,n}`$について、その積を$`Z=XY`$とする。この時、任意の$`L \in \R`$について下記が成り立つ。
    - $`\frac{\partial}{\partial X}L=(\frac{\partial}{\partial Z}L)Y^{\tau}`$
    - $`\frac{\partial}{\partial Y}L=X^{\tau}(\frac{\partial}{\partial Z}L)`$

---
# ベクトル微分における変数変換
$`u \in \R^m`$に対して、同じかより少ない次元$`l \le m`$で定義されるパラメータ$`t \in \R^l`$からの変換$`M \in \R^{m,l}`$が存在し、$`u=Mt`$が成り立つとする。またここでは、$`M`$の列が線形独立である場合を扱う。

- $`M`$の列が線形独立であるから$`(M^{\tau}M)^{-1}`$が存在し、疑似逆行列$`M^+= (M^{\tau}M)^{-1}M^{\tau}`$は$`M`$の左逆元となるので、$`t=M^+u`$である。よって、
    - $`(\frac{\partial}{\partial u}t^{\tau})=M(M^{\tau}M)^{-1}`$

ここで、$`v \in \R^n`$の$`u`$による微分を$`t`$による微分で表すことを考える。

- 連鎖律より、
    - $`\frac{\partial}{\partial u}v^{\tau}=(\frac{\partial}{\partial u}t^{\tau})(\frac{\partial}{\partial t}v^{\tau})`$
- $`u=Mt`$を代入し、
    - $`\frac{\partial}{\partial (Mt)}v^{\tau}=M(M^{\tau}M)^{-1}\frac{\partial}{\partial t}v^{\tau}`$
- 特に、$`v=t`$である場合は、
    - $`\frac{\partial}{\partial (Mv)}v^{\tau}=M(M^{\tau}M)^{-1}`$

---
# テーラー展開

$`x \in \R^n,f(x) \in \R`$について、$`x=v`$まわりで２次までのテーラー展開を行うと下記になる。

- $`f(x)=f(v)+(x-v)^{\tau}f'(v) + \frac{1}{2}(x-v)^{\tau}H(v)(x-v)`$
    - $`f'(x) \in \R^n`$は、関数$`f`$のxによる偏微分を成分にもつベクトル$`f'(x) = \frac{\partial f}{\partial x}`$
    - $`H(x) \in \R^{n,n}`$は、関数$`f`$のヘッセ行列（xによる2階偏微分を成分にもつ行列）$`H(x)=\frac{\partial^2 f}{\partial x\partial x^{\tau}}`$である。
