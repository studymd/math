# 定義

## 尤度関数とは

確率変数Xもしくは独立同分布確率変数列$`X_1,\cdots,X_n`$に対し、その確率分布がパラメータ$`\theta`$で決まる場合に定義できる写像$`L_X(\theta)`$であって、確率密度関数$`f_X(x;\theta)`$もしくは確率質量関数$`f_X(x;\theta)`$をもとにして定義される

- 確率変数Xの尤度関数
    - $`L_X(\theta;x) \equiv f_X(x;\theta)`$
    - $`f_X(x;\theta)`$と形は同じであるが、何を引数と捉えるかの違いがある。尤度関数は$`f_X(x;\theta)`$を$`\theta`$の関数であると見た関数である。
- 標本尤度関数（独立同分布確率変数列$`X_1,\cdots,X_n`$の尤度関数）
    - $`L_X(\theta;x_1,\cdots,x_n) \equiv {\displaystyle \prod_{1 \leq k \leq n}}f_X(x_k;\theta)`$
    - 尤度関数の$`x_1,\cdots,x_n`$に任意標本$`X_1,\cdots,X_n`$を代入して得られる関数$`L_X(\theta;X_1,\cdots,X_n)`$は確率的に決まる関数となり、任意標本尤度関数と呼ぶ
    - 尤度関数の$`x_1,\cdots,x_n`$に標本データ$`d_1,\cdots,d_n`$（定数の列）を代入して得られる関数$`L_x(\theta)`$は、標本データの下でのパラメータ$`\theta`$の尤度を表す関数となり、標本データ尤度関数と呼ぶ

## 対数尤度関数とは
尤度関数の対数を取った関数のこと

## スコア関数（対数尤度を偏微分した関数）とは
対数尤度関数をパラメータ$`\theta`$で偏微分した関数のこと

- 確率変数Xのスコア関数
    - $`S_X(\theta;x) \equiv \frac{\partial}{\partial\theta}logL_X(\theta;x)`$
- 標本（独立同分布確率変数列$`X_1,\cdots,X_n`$）のスコア関数
    - $`S_X(\theta;x_1,\cdots,x_n) \equiv \frac{\partial}{\partial\theta}logL_X(\theta;x_1,\cdots,x_n)=\frac{\partial}{\partial\theta}\sum_{k}log L_X(\theta;x_k)=\sum_{k}S_X(\theta;x_k)`$

## 対数尤度ヘッシアン（対数尤度を2次偏微分した関数）とは
対数尤度関数をパラメータ$`\theta`$で2階偏微分した関数のこと

- 確率変数Xの対数尤度ヘッシアン
    - $`H_X(\theta;x) \equiv \frac{\partial^2}{\partial\theta\partial\theta^{\tau}}logL_X(\theta;x)`$
- 標本（独立同分布確率変数列$`X_1,\cdots,X_n`$）の対数尤度ヘッシアン
    - $`H_X(\theta;x_1,\cdots,x_n) =\sum_{k}H_X(\theta;x_k)`$

---

# 性質

## 確率的に決まるスコア関数

スコア関数$`S_X(\theta;x)`$にXを代入して得られる確率的に決まる関数$`S_X(\theta;X)`$の値（確率変数）は下記の性質をもつ

- パラメータを含まない任意のXの数式$`g(X)`$に対して下記が成り立つ
    - $`E[g(X)S_X(\theta;X)]=\frac{\partial} {\partial\theta}E[g(X)]`$
        - 証明：$`E[g(X)S_X(\theta;X)]=E[g(X)\frac{\partial}{\partial\theta}logf(X;\theta)]=\int_{-\infty}^{\infty}f(x;\theta)g(x)\frac{\partial}{\partial\theta}logf(x;\theta)dx=\int_{-\infty}^{\infty}g(x)\frac{\partial}{\partial\theta}f(x;\theta)dx=\frac{\partial}{\partial\theta}\int_{-\infty}^{\infty}g(x)f(x;\theta)dx=\frac{\partial}{\partial\theta}E[g(X)]`$
- 期待値はゼロ：$`E[S_X(\theta;X)]=0`$
    - 証明：上式で$`g(x)=1`$とおけば直ちに得られる。
        - $`E[S_X(\theta;X)]=\frac{\partial}{\partial\theta}E[1]=0`$
- 分散は[フィッシャー情報量](../operator/fisher_information.md)に等しい：$`V[S_X(\theta;X)]=\mathcal{I}_{X}(\theta)`$
    - フィッシャー情報量の定義そのもの

## 確率的に決まる標本スコア関数

標本スコア関数に任意標本（確率変数の列）を代入して得られる確率的に決まる関数$`S_X(\theta;X_1,\cdots,X_n)`$の値（確率変数）は、nが十分に大きければフィッシャー情報量を分散とする正規分布に従う

- $`S_X(\theta;X_1,\cdots,X_n) \sim N(0,\mathcal{I}_{X \times n}(\theta))`$
- 証明
    - 任意標本$`X_1,...,X_n`$をそれぞれスコア関数に代入して得られる確率変数$`S_X(\theta;X_1),\cdots,S_X(\theta;X_n)`$を$'\theta'$に依存する新たな任意標本$`Y_1(\theta),...,Y_n(\theta)`$だと考えてみる
    - するとその標本平均$`\bar{Y}=\frac{1}{n}\sum{Y_k}`$について下記の等式が成り立つ
        - $`\bar{Y}(\theta) = \frac{1}{n}\Sigma \frac{\partial}{\partial\theta}logf(X_k;\theta) = \frac{1}{n}\frac{\partial}{\partial\theta}\Sigma logf(X_k;\theta) = \frac{1}{n}S_X(\theta;X_1,\cdots,X_n)`$
    - nが十分に大きければ、[中心極限定理](../base/central_limit_theorem.md)より
        - $`\bar{Y}(\theta) \sim N(E[Y_k],\frac{1}{n}V[Y_k]) = N(0,\frac{1}{n}\mathcal{I}_{X_1}(\theta))`$
    - 以上より
        - $`S_X(\theta;X_1,\cdots,X_n) = n \bar{Y}(\theta) \sim N(0,n\mathcal{I}_{X_1}(\theta))`$

## 確率的に決まる対数尤度ヘッシアン

対数尤度ヘッシアン$`H_X(\theta;x)`$にXを代入した関数$`H_X(\theta;X)`$の値（確率変数）の期待値の-1倍はスコア関数にXを代入した関数の値（確率変数）の分散と等しい

- $`-E[H_X(\theta;X)]=V[S_X(\theta;X)]`$

- 証明：尤度関数・スコア関数・対数尤度ヘッシアンにそれぞれ確率変数Xを代入した関数をそれぞれ$`L,S,H`$と略記する
    - (1)：一般に下記が成り立つ
        - $`\frac{\partial}{\partial\theta_i}\frac{\partial}{\partial\theta_j}logL=\frac{\partial}{\partial\theta_i}(\frac{1}{L}\frac{\partial}{\partial\theta_j}L)=-\frac{1}{L^2}(\frac{\partial}{\partial\theta_i}L)(\frac{\partial}{\partial\theta_j}L)+\frac{1}{L}\frac{\partial}{\partial\theta_i}\frac{\partial}{\partial\theta_j}L=-(\frac{\partial}{\partial\theta_i}logL)(\frac{\partial}{\partial\theta_j}logL)+\frac{1}{L}\frac{\partial}{\partial\theta_i}\frac{\partial}{\partial\theta_j}L`$
    - (2)：正則条件（微分と積分が交換できる）のもとで確率の定義より
        - $`E[\frac{1}{L}\frac{\partial}{\partial\theta_i}\frac{\partial}{\partial\theta_j}L]=\int\frac{\partial}{\partial\theta_i}\frac{\partial}{\partial\theta_j}Ldx=\frac{\partial}{\partial\theta_i}\frac{\partial}{\partial\theta_j}1=0`$
    - (3)：(1)と(2)より
        - $`E[\frac{\partial}{\partial\theta_i}\frac{\partial}{\partial\theta_j}logL] = E[-(\frac{\partial}{\partial\theta_i}logL)(\frac{\partial}{\partial\theta_j}logL)+\frac{1}{L}\frac{\partial}{\partial\theta_i}\frac{\partial}{\partial\theta_j}L]=-E[(\frac{\partial}{\partial\theta_i}logL)(\frac{\partial}{\partial\theta_j}logL)]`$
    - (4)：(3)の左辺をi行j列並べた行列は$`E[H]`$、右辺をi行j列並べた行列は$`-E[SS^{\tau}]`$になるので
        - $`V[S] = E[SS^{\tau}]=-E[H]`$
