# 定義

## 集合代数（algebra of sets）とは
（ある集合S上の）部分集合族$`M \subset 2^S`$であって、下記の集合演算上の要件を満たすもののこと。（この２つは同値な概念の別の捉え方であり同値である）

- 有限加法族の定義を満たす
- 集合体の定義を満たす

すなわち集合代数と、有限加法族あるいは集合体は同じ意味である。

### 部分集合族（family of subsets）とは
「ある集合の部分集合」を要素とする集合のこと。集合Sの部分集合を要素とする集合は、Sの部分集合族（もしくはS上の部分集合族）と呼ぶ。

### 集合演算とは
１もしくは複数の集合を引数にとり集合を返す演算のこと。代表的な集合演算としては、２項演算では合併（$`A \cup B`$）・交叉（$`A \cap B`$）・対称差（$`A \triangle B`$）・差（A \ B,もしくはA ー B）、単項演算では補（$`A^C`$）が存在する。

## 有限加法族（finitely additive class）とは
集合S上の部分集合族Mが、空でなく、合併演算（$`\cup`$）と補演算（$`{}^C`$）について閉じていること。※なお、補演算における全体集合は集合Sとする。

- $`\emptyset \neq M`$
- $`A, B \in M ⇒ A \cup B \in M`$
- $`A \in M ⇒ A^c \in M`$

上記定義より、自動的に下記を満たす

- $`S ∈ M`$
- $`\emptyset \in M`$
- $`A, B \in M ⇒ A \cap B \in M`$
- $`A, B \in M ⇒ A \backslash B \in M`$
- $`A, B \in M ⇒ A \triangle B \in M`$

なぜなら、
- $`S = A \cup A^c ∈ M`$
- $`\emptyset = (A \cup A^c)^c \in M`$
- $`A \cap B = (A^c \cup B^c)^c \in M`$
- $`A \backslash B = (A^c \cup B)^c \in M`$
- $`A \triangle B = (A^c \cup B)^c \cup (A \cup B^c)^c \in M`$

## 集合体（field of sets）とは
集合S上の部分集合族Mが、Sを要素として含み、交叉演算（$`\cap`$）と対称差演算（$`\triangle`$）について閉じていること。

- $`S \in M`$
- $`A, B \in M ⇒ A \cap B \in M`$
- $`A \in M ⇒ A \triangle B \in M`$

上記定義より、自動的に下記を満たす

- $`\emptyset \in M`$
- $`A \in M ⇒ A^c \in M`$
- $`A, B \in M ⇒ A \cup B \in M`$
- $`A, B \in M ⇒ A \backslash B \in M`$

なぜなら、
- $`\emptyset = S \triangle S \in M`$
- $`A^c = S \triangle A \in M`$
- $`A \cup B = (A^c \cap B^c)^c \in M`$
- $`A \backslash B = A \cap (A \triangle B) \in M`$

なお、Mについて加法を$`\triangle`$、乗法を$`\cap`$とすれば、後に示すように部分集合族と演算の組$`(M,\cap,\triangle)`$は可換環の定義を見たしている（集合体という名前だが体の定義は満たさない）

## $`\sigma`$-集合代数（sigma - algebra of sets）、$`\sigma`$-集合体（sigma - field of sets）、完全加法族（completely additive class）とは

集合代数の中でも特に、可算無限回の集合演算についても閉じているものを指す。加算加法族、完全加法族、$`\sigma`$-集合体も同じ概念を指す。

---
# 集合代数の例
集合S上の部分集合族Mについて、例えば下記は集合代数である。

- $`M=\{\emptyset,S\}`$
- 任意の$X \in S$について、$`M=\{\emptyset,\{X\},\{X\}^c,S\}`$
- 任意の$A \subset S$について、$`M=\{\emptyset,A,A^c,S\}`$

---
# 性質

集合S上の集合代数M（$`M \subset 2^S`$）は下記の性質をもつ。

- $`S ∈ M`$
- $`\emptyset \in M`$
- $`(M,\triangle)`$は、単位元を$`\emptyset`$、逆元を自分自身とする、アーベル群（＝可換群）である。吸収元は持たない。
- $`(M,\cap)`$は、単位元を$`S`$とする、可換モノイドである（逆元を持たない）。吸収元は$`\emptyset`$である。
- $`(M,\cup)`$は、単位元を$`\emptyset`$とする、可換モノイドである（逆元を持たない）。吸収元は$`S`$である。

加法を$`\triangle`$、乗法を$`\cap`$とすれば、下記の通り分配補足が成り立つので、集合台数と演算の組$`(M,\cap,\triangle)`$は、加法単位元（零元）を$`\emptyset`$、乗法単位元を$`S`$とする可換環である。$`\emptyset`$が乗法吸収元かつ加法単位元（零元）である。

- 分配法則
    - $`A, B, C \in M ⇒ (A \triangle B) \cap C = (A \cap C) \triangle (B \cap C)`$